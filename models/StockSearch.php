<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Stock;

/**
 * StockSearch represents the model behind the search form of `app\models\Stock`.
 */
class StockSearch extends Stock
{
    public $status;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stock_id', 'stock_number'], 'integer'],
            [['stock_name', 'stock_date', 'stock_detail','status','code'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stock::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'stock_id' => [
                    'asc' => ['stock_id' => SORT_ASC],
                    'desc' => ['stock_id' => SORT_DESC],
                ],
            ],
            'defaultOrder' => [
                'stock_id' => SORT_DESC
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'stock_id' => $this->stock_id,
            'stock_number' => $this->stock_number,
            'stock_date' => $this->stock_date,
            'status' => $this->status,
        ]);

        $query->andFilterHaving(['like', 'stock_name', $this->stock_name])
            ->orFilterHaving(['like', 'code', $this->stock_name]);

        return $dataProvider;
    }
}
