<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_meet".
 *
 * @property int $list_id
 * @property string $list_name
 *
 * @property MeetingHasListMeet[] $meetingHasListMeets
 * @property Meeting[] $meets
 */
class ListMeet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'list_meet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['list_name'], 'string', 'max' => 45],
            [['list_name'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'list_id' => 'List ID',
            'list_name' => 'List Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingHasListMeets()
    {
        return $this->hasMany(MeetingHasListMeet::className(), ['list_id' => 'list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeets()
    {
        return $this->hasMany(Meeting::className(), ['meet_id' => 'meet_id'])->viaTable('meeting_has_list_meet', ['list_id' => 'list_id']);
    }
}
