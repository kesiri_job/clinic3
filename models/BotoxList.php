<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "botox_list".
 *
 * @property int $id
 * @property string $botox_id_name
 * @property string $botox_left
 * @property string $botox_right
 * @property string $botox_u
 * @property int $meet_id
 *
 * @property Meeting $meet
 */
class BotoxList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'botox_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'meet_id'], 'integer'],
            [['botox_id_name', 'botox_u'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['meet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meeting::className(), 'targetAttribute' => ['meet_id' => 'meet_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'botox_id_name' => 'Botox Id Name',
            'botox_u' => 'Botox U',
            'meet_id' => 'Meet ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeet()
    {
        return $this->hasOne(Meeting::className(), ['meet_id' => 'meet_id']);
    }
    public function getBotoxName(){
        if($this->botox_u!=null){return $this->botox_id_name." ".$this->botox_u." cc.<br/>";
        }else{
            return null;
        }

    }
}
