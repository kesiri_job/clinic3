<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "work".
 *
 * @property int $id
 * @property int $user_id
 * @property string $work_list
 * @property string $work_time_start
 * @property string $work_time_end
 * @property string $work_detail
 * @property string $work_date
 * @property string $work_type
 * @property string $work_type2 พนักงาน/หมอ
 *
 * @property User $user
 */
class Work extends \yii\db\ActiveRecord
{
    const TYPE_DOCTOR = 2;
    const TYPE_EMPLOYEE = 1;
    const WORK_TYPE = 1;
    const WORK_LEAVE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['work_type', 'in', 'range' => [self::WORK_TYPE, self::WORK_LEAVE]],
            ['work_type2', 'in', 'range' => [self::TYPE_DOCTOR, self::TYPE_EMPLOYEE]],
            [['user_id', 'work_time_start', 'work_time_end', 'work_date', 'work_type', 'work_type2'], 'required'],
            [['user_id'], 'integer'],
            [['work_time_start', 'work_time_end', 'work_date'], 'safe'],
            [['work_type', 'work_type2'], 'string'],
            [['work_list', 'work_detail'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Userabt::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ชื่อ',
            'work_list' => 'รายละเอียด',
            'work_time_start' => 'เวลาเริ่มงาน',
            'work_time_end' => 'เวลาสิ้นสุดงาน',
            'work_detail' => 'หมายเหตุ',
            'work_date' => 'วันที่เข้างาน',
            'work_type' => 'รายการ',
            'work_type2' => 'ประเภทบุคคล',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getItemType(){
        return [
            self::WORK_TYPE => 'เข้างาน',
            self::WORK_LEAVE => 'ลางาน'
        ];
    }
    public function getTypeName()
    {
        $items = $this->getItemType();
        return array_key_exists($this->work_type, $items) ? $items[$this->work_type] : '';
    }
    public function getItemType2(){
        return [
            self::TYPE_DOCTOR => 'หมอ',
            self::TYPE_EMPLOYEE => 'พนักงาน'
        ];
    }
    public function getType2Name()
    {
        $items = $this->getItemType2();
        return array_key_exists($this->work_type2, $items) ? $items[$this->work_type2] : '';
    }
}
