<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "appointment".
 *
 * @property int $appoint_id
 * @property string $appoint_name
 * @property string $appoint_date
 * @property string $customer_id
 * @property string $appoint_detail
 * @property string $status
 * @property string $call
 * @property string $type_pay
 * @property int $deposit
 * @property int $full_price
 */
class Appointment extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const CALL_NOT_ACTIVE = 0;
    const CALL_ACTIVE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appointment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_NOT_ACTIVE]],
            ['call', 'in', 'range' => [self::CALL_ACTIVE, self::CALL_NOT_ACTIVE]],
            [['status','call'], 'string'],
            [['deposit', 'full_price'], 'integer'],
            [['appoint_name', 'appoint_date', 'customer_id'], 'string', 'max' => 45],
            [['appoint_detail'], 'string', 'max' => 255],
            [['appoint_date','appoint_name','status','call'], 'required'],
            [['type_pay'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'appoint_id' => 'Appoint ID',
            'appoint_name' => 'หัวข้อการเข้าพบ',
            'appoint_date' => 'วันที่นัดพบ',
            'customer_id' => 'ชื่อผู้เข้าพบ',
            'appoint_detail' => 'รายละเอียด',
            'status' => 'สถานะการเข้าพบ',
            'call' => 'สถานะการโทรตาม',
            'type_pay' => 'การจ่ายเงิน',
            'deposit' => 'ค่ามัดจำ',
            'full_price' => 'ราคาเต็ม',
        ];
    }
    public function getItemStatus(){
        return [
            self::STATUS_ACTIVE => 'เข้าพบแล้ว',
            self::STATUS_NOT_ACTIVE => 'ยังไม่เข้าพบ'
        ];
    }
    public function getStatusName()
    {
        $items = $this->getItemStatus();
        return array_key_exists($this->status, $items) ? $items[$this->status] : '';
    }
    public function getItemCall(){
        return [
            self::CALL_ACTIVE => 'โทรตามแล้ว',
            self::CALL_NOT_ACTIVE => 'ยังไม่โทรตาม'
        ];
    }
    public function getCallName()
    {
        $items = $this->getItemCall();
        return array_key_exists($this->call, $items) ? $items[$this->call] : '';
    }
    public function getCustomer(){
        return $this->hasOne(Customer::className(), ['customer_id' => 'customer_id']);
    }
}
