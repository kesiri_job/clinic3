<?php

namespace app\models;


use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "meeting".
 *
 * @property int $meet_id
 * @property string $meet_title
 * @property string $meet_date
 * @property string $meet_detail
 * @property string $summary
 * @property int $customer_id
 * @property int $doctor_id
 * @property string $status_2
 * @property string $result
 * @property string $proplem
 *
 * @property BotoxList[] $botoxLists
 * @property FatList[] $fatLists
 * @property Customer $customer
 * @property Doctor $doctor
 * @property MeetingHasListMeet[] $meetingHasListMeets
 * @property ListMeet[] $lists
 */
class Meeting extends \yii\db\ActiveRecord
{
    public $file;
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS2_NOT_ACTIVE = 0;
    const STATUS2_ACTIVE = 1;


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'file' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'picture',
            ]
        ];
    }

    public static function tableName()
    {
        return 'meeting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_NOT_ACTIVE]],
            [['file', 'meet_date'], 'safe'],
            [['status','status_2'], 'string'],
            [['customer_id', 'meet_date', 'doctor_id'], 'required'],
            [['customer_id', 'doctor_id', 'summary', 'meet_id_edit'], 'integer'],
            [['meet_title', 'meet_detail', 'picture', 'result', 'proplem'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'customer_id']],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::className(), 'targetAttribute' => ['doctor_id' => 'doctor_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'meet_id' => 'Meet ID',
            'meet_title' => 'หัวข้อการเข้าพบ',
            'meet_date' => 'วันที่เข้าพบ',
            'meet_detail' => 'รายละเอียดการเข้าพบ',
            'meet_list' => 'รายการที่ทำ',
            'customer_id' => 'ชื่อ',
            'doctor_id' => 'ชื่อหมอ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotoxLists()
    {
        return $this->hasMany(BotoxList::className(), ['meet_id' => 'meet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFatLists()
    {
        return $this->hasMany(FatList::className(), ['meet_id' => 'meet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::className(), ['doctor_id' => 'doctor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingHasListMeets()
    {
        return $this->hasMany(MeetingHasListMeet::className(), ['meet_id' => 'meet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLists()
    {
        return $this->hasMany(ListMeet::className(), ['list_id' => 'list_id'])->viaTable('meeting_has_list_meet', ['meet_id' => 'meet_id']);
    }

    public static function findListId($list_id, $meet_id)
    {
        if (MeetingHasListMeet::find()->where(['meet_id' => $meet_id, 'list_id' => $list_id])->one()) {
            return MeetingHasListMeet::find()->where(['meet_id' => $meet_id, 'list_id' => $list_id])->one()->list_id;
        } else {
            return false;
        }
    }

    public function getImage()
    {
        return $this->picture ? Yii::getAlias('@web/uploads/') . $this->picture : Yii::getAlias('@web/uploads/nopic.png');
    }

    public function getImagesum()
    {
        return $this->picture ? Yii::getAlias('@web/uploads/') . $this->picture : "";
    }

    public function getItemStatus(){
        return [
            self::STATUS_ACTIVE => 'เคสปกติ',
            self::STATUS_NOT_ACTIVE => 'เคสแก้ไข'
        ];
    }
    public function getStatusName()
    {
        $items = $this->getItemStatus();
        return array_key_exists($this->status, $items) ? $items[$this->status] : '';
    }

    public function getItemStatus2(){
        return [
            self::STATUS2_ACTIVE => 'เสร็จสิ้น',
            self::STATUS2_NOT_ACTIVE => 'กำลังรักษา'
        ];
    }
    public function getStatusName2()
    {
        $items = $this->getItemStatus2();
        return array_key_exists($this->status_2, $items) ? $items[$this->status_2] : '';
    }


}
