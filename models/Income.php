<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "income".
 *
 * @property int $income_id
 * @property string $income_name
 * @property int $income_number
 * @property string $income_date
 * @property int $create_by
 * @property string $detail
 */
class Income extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
        ];
    }
    public static function tableName()
    {
        return 'income';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['income_name', 'income_number', 'income_date'], 'required'],
            [['income_number', 'created_by','updated_by'], 'integer'],
            [['income_date'], 'safe'],
            [['income_name', 'detail'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'income_id' => 'Income ID',
            'income_name' => 'รายรับ',
            'income_number' => 'จำนวนเงิน',
            'income_date' => 'วันที่',
            'detail' => 'รายละเอียด',
            'created_by' => 'ผู้เพิ่มข้อมูล',
            'updated_by'=>'ผู้แก้ไขข้อมูล',
        ];
    }
}
