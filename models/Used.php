<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "used".
 *
 * @property int $id
 * @property string $name
 * @property int $number
 * @property int $user_id
 * @property string $detail
 * @property string $date
 */
class Used extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'used';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'number', 'user_id'], 'required'],
            [['number', 'user_id'], 'integer'],
            [['date'], 'safe'],
            [['name', 'detail'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ชื่อการใช้ยา',
            'number' => 'จำนวน',
            'user_id' => 'ผู้เบิกยา',
            'detail' => 'รายละเอียด',
            'date' => 'วันที่',
        ];
    }
}
