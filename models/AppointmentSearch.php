<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Appointment;

/**
 * AppointmentSearch represents the model behind the search form of `app\models\Appointment`.
 */
class AppointmentSearch extends Appointment
{
    public $status;
    public $call;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['appoint_id'], 'integer'],
            [['appoint_name', 'appoint_date', 'customer_id','status','call'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Appointment::find()->orderBy('appoint_id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'appoint_id' => $this->appoint_id,
            'status' => $this->status,
            'call'=>$this->call
        ]);

        $query->andFilterWhere(['like', 'appoint_name', $this->appoint_name])
            ->andFilterWhere(['like', 'appoint_date', $this->appoint_date])
            ->andFilterWhere(['like', 'customer_id', $this->customer_id]);

        return $dataProvider;
    }
}
