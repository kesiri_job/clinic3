<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Meeting;

/**
 * MeetSearch represents the model behind the search form of `app\models\Meeting`.
 */
class MeetSearch extends Meeting
{
    public $status;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meet_id', 'customer_id', 'summary'], 'integer'],
            [['meet_title', 'meet_date', 'meet_detail','status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$customer_id)
    {
        $query = Meeting::find()->where(['customer_id'=>$customer_id])->orderBy('meet_id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'meet_id' => $this->meet_id,
            'customer_id' => $this->customer_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'meet_title', $this->meet_title])
            ->andFilterWhere(['like', 'meet_detail', $this->meet_detail])
            ->andFilterWhere(['like', 'meet_date', $this->meet_date]);


        return $dataProvider;
    }
}
