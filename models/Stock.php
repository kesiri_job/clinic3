<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stock".
 *
 * @property int $stock_id
 * @property string $stock_name
 * @property int $stock_number
 * @property string $stock_date
 * @property string $code
 * @property string $stock_detail
 * @property int $user_id
 */
class Stock extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_NOT_ACTIVE]],
            [['stock_number', 'user_id','left'], 'integer'],
            [['stock_date'], 'safe'],
            [['status','code'], 'string'],
            [['user_id','status','code'], 'required'],
            [['stock_name'], 'string', 'max' => 200],
            [['stock_detail'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'stock_id' => 'Stock ID',
            'stock_name' => 'ชื่อ',
            'stock_number' => 'จำนวนนำเข้า',
            'left'=>'จำนวนที่เหลือ',
            'stock_date' => 'วันที่นำเข้า',
            'stock_detail' => 'รายละเอียด',
            'user_id' => 'User ID',
            'code' => 'รหัส',
        ];
    }
    public function getItemStatus(){
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_NOT_ACTIVE => 'Not Active'
        ];
    }
    public function getStatusName()
    {
        $items = $this->getItemStatus();
        return array_key_exists($this->status, $items) ? $items[$this->status] : '';
    }
    public function getCode()
    {
        return $this->hasOne(Drug::className(), ['code' => 'code']);
    }
}
