<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fat".
 *
 * @property int $fat_id
 * @property string $fat_name
 *
 * @property Meeting[] $meetings
 */
class Fat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fat_name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fat_id' => 'Fat ID',
            'fat_name' => 'Fat Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetings()
    {
        return $this->hasMany(Meeting::className(), ['fat_id' => 'fat_id']);
    }
}
