<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detail_user_meet".
 *
 * @property int $id
 * @property int $user_id
 * @property int $detail_id
 */
class DetailUserMeet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detail_user_meet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'detail_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'detail_id' => 'Detail ID',
        ];
    }

    public function getUsers()
    {
        return $this->hasMany(Userabt::className(), ['id' => 'user_id']);
    }

}
