<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "botox".
 *
 * @property int $botox_id
 * @property string $botox_name
 */
class Botox extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'botox';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['botox_name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'botox_id' => 'Botox ID',
            'botox_name' => 'Botox Name',
        ];
    }
}
