<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customer;

/**
 * CustomerSearch represents the model behind the search form of `app\models\Customer`.
 */
class CustomerSearch extends Customer
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'age'], 'integer'],
            [['customer_name', 'phone', 'picture', 'nicname', 'gender', 'career', 'disease', 'id_card', 'history_allergy', 'birth_dath'], 'safe'],
            [['height', 'weight'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'customer_id' => [
                    'asc' => ['customer_id' => SORT_ASC],
                    'desc' => ['customer_id' => SORT_DESC],
                ],

            ],
            'defaultOrder' => [
                'customer_id' => SORT_DESC
            ]
        ]);
        $dataProvider->sort->attributes['customer_name'] = [
            'desc' => ['customer_name' => SORT_DESC],
            'asc' => ['customer_name' => SORT_ASC],

        ];
        $dataProvider->sort->attributes['id_card'] = [
            'desc' => ['id_card' => SORT_DESC],
            'asc' => ['id_card' => SORT_ASC],

        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'customer_id' => $this->customer_id,
            'age' => $this->age,
            'height' => $this->height,
            'weight' => $this->weight,
            'birth_dath' => $this->birth_dath,
        ]);
        $query->andFilterWhere(['OR' ,
            [ 'like' , 'customer_name' , $this->id_card ],
            [ 'like' , 'customer_name' , $this->customer_name ],
        ]);

        return $dataProvider;
    }
}
