<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/style.css',
        'https://use.fontawesome.com/releases/v5.0.13/css/all.css',
        'template/css/style.css',
        'template/css/helper.css',
        'template/css/animate.css',
        'template/css/bootstrap-reset.css',
        'template/ionicon/css/ionicons.min.css',
        'template/material-design-iconic-font/css/material-design-iconic-font.min.css',
        'https://fonts.googleapis.com/css?family=Kanit'
    ];
    public $js = [
//        'js/googlemap.js',
//        'js/Chart.min.js',
//        'js/app.js',
//        'template/js/jquery.app.js',
//        'template/js/pace.min.js',
//        'template/js/modernizr.min.js',
//        'template/js/wow.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
