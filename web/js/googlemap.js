var map;
var marker = [];
var gmarkers = [];
var housemarker = []
var infowindow ;

function initMap() {
		if(localStorage.lat != null && localStorage.lng != null){
			var latStr = localStorage.lat;
			var lngStr = localStorage.lng;
		}else{
			var latStr = "16.494957199999998";
			var lngStr = "102.8419682";
		}
	
		var uluru = {lat: parseFloat(latStr), lng: parseFloat(lngStr)};
		$("#lat").val(latStr);
		$("#lng").val(lngStr);
		$("#latShow").val(latStr);
    	$("#lngShow").val(lngStr);
		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 18,
			center: uluru,
			mapTypeId: 'roadmap'
		});
		marker = new google.maps.Marker({
			position: uluru,
			map: map,
			draggable:true
		});
		gmarkers.push(marker);
		infowindow = new google
			.maps
			.InfoWindow();

		google
			.maps
			.event
			.addListener(marker, 'dragend', function () {
			    var my_Point = marker.getPosition(); // หาตำแหน่งของตัว marker เมื่อกดลากแล้วปล่อย
			    map.panTo(my_Point); // ให้แผนที่แสดงไปที่ตัว marker
				$("#lat").val(my_Point.lat())
				$("#lng").val(my_Point.lng())
				$("#latShow").val(my_Point.lat())
				$("#lngShow").val(my_Point.lng())
			});
}

function setCenter() {
	var mapShow;
	mapShow = map;
	for(i=0; i<gmarkers.length; i++){
		gmarkers[i].setMap(null);
	}

    if (navigator.geolocation) {
        navigator
            .geolocation
            .getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
				};
				
                var marker = new google
                    .maps
                    .Marker({
                        position: {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        },
                        map: mapShow,
                        title: 'Maker',
                        draggable: true
                    });
                
				gmarkers.push(marker);
                google
                    .maps
                    .event
                    .addListener(marker, 'dragend', function () {
                        var my_Point = marker.getPosition(); // หาตำแหน่งของตัว marker เมื่อกดลากแล้วปล่อย
                        mapShow.panTo(my_Point); // ให้แผนที่แสดงไปที่ตัว marker
						$("#lat").val(my_Point.lat())
						$("#lng").val(my_Point.lng())
                    });

				mapShow.setMapTypeId('roadmap');
				mapShow.setCenter(pos);
				mapShow.setZoom(17);
                $("#lat").val(pos.lat);
				$("#lng").val(pos.lng);
				$("#latShow").val(pos.lat);
    			$("#lngShow").val(pos.lng);

            }, function () {
                alert("อุปกรณ์ของคุณไม่สนับสนุนระบบ GPS Geolocation")
            });
    } else {
        // Browser doesn't support Geolocation
        alert("อุปกรณ์ของคุณไม่สนับสนุนระบบ GPS Geolocation")
    }
}