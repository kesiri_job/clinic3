<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/23/2018
 * Time: 11:27 PM
 */

namespace app\controllers;


use app\models\Drug;
use app\models\Stock;
use app\models\StockSearch;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class StockController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Stock models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStockIn()
    {
        $model = Drug::find()->all();
        if (Yii::$app->request->isAjax) {
            // $data = Yii::$app->request->post();
            $code = \Yii::$app->request->post('code');
            $name = \Yii::$app->request->post('name');
            try {
                $model = new Drug(['code' => $code, 'name' => $name]);
                if ($model->save(false)) {
                    Yii::$app->getSession()->setFlash('alert1', [
                        'type' => 'success',
                        'duration' => 10000,
                        'icon' => 'fas fa-check-circle',
                        'title' => Yii::t('app', Html::encode('Success')),
                        'message' => Yii::t('app', Html::encode('บันทึกสำเร็จ')),
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                }
            } catch (Exception $e) {
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'danger',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app', Html::encode('ขออภัย รหัสยานี้มีข้อมูลในระบบแล้ว')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->render('input', [
                'model' => $model
            ]);
        }
    }

    /**
     * Displays a single Stock model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Stock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionExport($id)
    {
        $model = Stock::findOne($id);
        try {
            if($model) {
                if ($model->left > 0) {
                    $model->left = $model->left - 1;
                    if ($model->save()) {
                        Yii::$app->getSession()->setFlash('alert1', [
                            'type' => 'success',
                            'duration' => 10000,
                            'icon' => 'fas fa-check-circle',
                            'title' => Yii::t('app', Html::encode('Success')),
                            'message' => Yii::t('app', Html::encode('ลดจำนวนยารหัส ' . $model->code . ' สำเร็จ')),
                            'positonY' => 'top',
                            'positonX' => 'right'
                        ]);
                    }
                } else {
                    Yii::$app->getSession()->setFlash('alert1', [
                        'type' => 'danger',
                        'duration' => 10000,
                        'icon' => 'fas fa-exclamation-triangle',
                        'title' => Yii::t('app', Html::encode('Warning')),
                        'message' => Yii::t('app', Html::encode('ขออภัย จำนวนยาเหลือ 0 แล้ว')),
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                }
            }else{
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'danger',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app', Html::encode('มีข้อผิดพลาด')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
        } catch (Exception $e) {
            Yii::$app->getSession()->setFlash('alert1', [
                'type' => 'danger',
                'duration' => 10000,
                'icon' => 'fas fa-exclamation-triangle',
                'title' => Yii::t('app', Html::encode('Warning')),
                'message' => Yii::t('app', Html::encode('มีข้อผิดพลาด')),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }
        return $this->redirect(['index']);
    }
    public function actionPlus($id)
    {
        $model = Stock::findOne($id);
        try {
            if($model) {
//                if ($model->left<$model->stock_number) {
                    $model->left = $model->left + 1;
                    if ($model->save()) {
                        Yii::$app->getSession()->setFlash('alert1', [
                            'type' => 'success',
                            'duration' => 10000,
                            'icon' => 'fas fa-check-circle',
                            'title' => Yii::t('app', Html::encode('Success')),
                            'message' => Yii::t('app', Html::encode('ลดจำนวนยารหัส ' . $model->code . ' สำเร็จ')),
                            'positonY' => 'top',
                            'positonX' => 'right'
                        ]);
                    }
//                } else {
//                    Yii::$app->getSession()->setFlash('alert1', [
//                        'type' => 'danger',
//                        'duration' => 10000,
//                        'icon' => 'fas fa-exclamation-triangle',
//                        'title' => Yii::t('app', Html::encode('Warning')),
//                        'message' => Yii::t('app', Html::encode('ขออภัย จำนวนยาเกินจำนวนนำเข้า')),
//                        'positonY' => 'top',
//                        'positonX' => 'right'
//                    ]);
//                }
            }else{
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'danger',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app', Html::encode('มีข้อผิดพลาด')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
        } catch (Exception $e) {
            Yii::$app->getSession()->setFlash('alert1', [
                'type' => 'danger',
                'duration' => 10000,
                'icon' => 'fas fa-exclamation-triangle',
                'title' => Yii::t('app', Html::encode('Warning')),
                'message' => Yii::t('app', Html::encode('มีข้อผิดพลาด')),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }
        return $this->redirect(['index']);
    }
    public function actionCreate()
    {
        $model = new Stock();
        $check = false;
        if (Yii::$app->request->isAjax) {
            $code = \Yii::$app->request->post('code');
            $model2 = Drug::findOne($code)->name;
            if ($model2) {
                $check = true;
            }
            return $model2;
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'fas fa-check-circle',
                    'title' => Yii::t('app', Html::encode('Success')),
                    'message' => Yii::t('app', Html::encode('บันทึกสำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                if ($check == false) {
                    Yii::$app->getSession()->setFlash('alert1', [
                        'type' => 'danger',
                        'duration' => 10000,
                        'icon' => 'fas fa-exclamation-triangle',
                        'title' => Yii::t('app', Html::encode('Warning')),
                        'message' => Yii::t('app', Html::encode('มีข้อผิดพลาด ไม่พบรหัสยานี้')),
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                } else {
                    Yii::$app->getSession()->setFlash('alert1', [
                        'type' => 'warning',
                        'duration' => 10000,
                        'icon' => 'fas fa-exclamation-triangle',
                        'title' => Yii::t('app', Html::encode('Warning')),
                        'message' => Yii::t('app', Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                }
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Stock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'fas fa-check-circle',
                    'title' => Yii::t('app', Html::encode('Success')),
                    'message' => Yii::t('app', Html::encode('บันทึกสำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'warning',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app', Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Stock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('alert1', [
            'type' => 'success',
            'duration' => 10000,
            'icon' => 'fas fa-check-circle',
            'title' => Yii::t('app', Html::encode('Success')),
            'message' => Yii::t('app', Html::encode('ลบข้อมูลสำเร็จ')),
            'positonY' => 'top',
            'positonX' => 'right'
        ]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Stock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stock::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}