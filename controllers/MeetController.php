<?php

namespace app\controllers;

use app\models\BotoxList;
use app\models\ModelT;
use app\models\Customer;
use app\models\Fat;
use app\models\FatList;
use app\models\ListMeet;
use app\models\MeetingHasListMeet;
use kartik\form\ActiveForm;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use app\models\Meeting;
use app\models\MeetSearch;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * MeetController implements the CRUD actions for Meeting model.
 */
class MeetController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                    // ...
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-delete',
            ],
            'avatar-delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }
    /**
     * Lists all Meeting models.
     * @return mixed
     */

    public function actionIndex($id)
    {

        $searchModel = new MeetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        $modelcustomer = Customer::findOne($id);
        $query = (new Query())->from('meeting');
        $sum = $query->where(['customer_id'=>$id])->sum('summary');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'customer' => $modelcustomer,
            'customer_id' => $id,
            'sum'=>$sum ?$sum:0
        ]);
    }

    /**
     * Displays a single Meeting model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPhoto($id)
    {

        $photo = Meeting::find()->select(['picture','meet_date','meet_id','status'])->where(['customer_id'=>$id])->orderBy('meet_date desc')->all();
        return $this->render('photo', [
            'id' => $id,
            'photo'=>$photo,

        ]);
    }
    public function actionView($id)
    {
        $meetHasList = MeetingHasListMeet::find()->where(['meet_id' => $id])->all();
        $fatList = FatList::find()->where(['meet_id' => $id])->all();
        $botoxList = BotoxList::find()->where(['meet_id' => $id])->all();
        $customer = Yii::$app->request->get('customer');
        foreach ($meetHasList as $item) {
            $array[] = implode(',', ArrayHelper::map(ListMeet::find()->where(['list_id' => $item->list_id])->all(), 'list_id', 'list_name'));
        }
        $arrayFat[] = implode('', ArrayHelper::map(FatList::find()->where(['meet_id' => $id])->all(), 'fatName', 'fatName'));//มึงเขียนไว้ใน Model
        $arrayBotox[] = implode('', ArrayHelper::map(BotoxList::find()->where(['meet_id' => $id])->all(), 'botoxName', 'botoxName'));
        return $this->render('view', [
            'model' => $this->findModel($id),
            'customer' => $customer,
            'meetHasList' => $meetHasList,
            'fatList' => $fatList,
            'botoxList' => $botoxList,
            'array' => empty($array) ? null : $array,
            'arrayFat' => $arrayFat,
            'arrayBotox' => $arrayBotox,
        ]);
    }

    /**
     * Creates a new Meeting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Meeting();
        $modelsFatlist = [new FatList];
        $modelsBotoxlist = [new BotoxList];
        //  $modelcustomer = Customer::findOne($id);
        $customer = Yii::$app->request->get('id');

        if ($model->load(Yii::$app->request->post())) {
            $model->customer_id = $customer;
            if ($model->status==0){
                $active = "edit";
            }else{
                $active = "";
            }
            $modelsFatlist = ModelT::createMultiple(FatList::classname());
            ModelT::loadMultiple($modelsFatlist, Yii::$app->request->post());

            $oldIDs2 = ArrayHelper::map($modelsBotoxlist, 'id', 'id');
            $modelsBotoxlist = ModelT::createMultiple(BotoxList::classname(), $modelsBotoxlist);
            ModelT::loadMultiple($modelsBotoxlist, Yii::$app->request->post());
            $deletedIDs2 = array_diff($oldIDs2, array_filter(ArrayHelper::map($modelsBotoxlist, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = ModelT::validateMultiple($modelsFatlist) && $valid;

            $valid2 = $model->validate();
            $valid2 = ModelT::validateMultiple($modelsBotoxlist) && $valid2;

            if ($valid) {

                $transaction = \Yii::$app->db->beginTransaction();
                try {

                    if ($flag = $model->save(false)) {

                        try {
                            $this->deleteCasecade($model->meet_id);
                        } catch (Exception $e) {
                            echo $e;
                        }
                        foreach ($modelsFatlist as $modelsFatlist) {
                            $modelsFatlist->meet_id = $model->meet_id;
                            if (!($flag = $modelsFatlist->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelsBotoxlist as $modelsBotoxlist) {
                            $modelsBotoxlist->meet_id = $model->meet_id;
                            if (!($flag = $modelsBotoxlist->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->getSession()->setFlash('alert1', [
                            'type' => 'success',
                            'duration' => 10000,
                            'icon' => 'fas fa-check-circle',
                            'title' => Yii::t('app', Html::encode('Success')),
                            'message' => Yii::t('app', Html::encode('บันทึกสำเร็จ')),
                            'positonY' => 'top',
                            'positonX' => 'right'
                        ]);
                        return $this->redirect(['meet/' . $model->meet_id, 'customer' => $customer,'status'=>$active]);
                    }
                } catch (Exception $e) {
                    Yii::$app->getSession()->setFlash('alert1', [
                        'type' => 'warning',
                        'duration' => 10000,
                        'icon' => 'fas fa-exclamation-triangle',
                        'title' => Yii::t('app', Html::encode('Warning')),
                        'message' => Yii::t('app', Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);
                    $transaction->rollBack();
                }
            }

        }

        return $this->render('create', [
            'model' => $model,
            'customer' => $id,
            'modelsFatlist' => (empty($modelsFatlist)) ? [new FatList()] : $modelsFatlist,
            'modelsBotoxlist' => (empty($modelsBotoxlist)) ? [new BotoxList] : $modelsBotoxlist,
        ]);
    }

    public function deleteCasecade($id)
    {
        MeetingHasListMeet::deleteAll(['meet_id' => $id]);
        if (Yii::$app->request->post('Meeting')['lists']) {
            foreach (Yii::$app->request->post('Meeting')['lists'] as $item) {
                if (!Meeting::findListId($item, $id)) {//ถ้าไม่มีจะบันทึกลง
                    if (!is_numeric($item)) {
                        $List = new ListMeet();
                        $List->list_name = $item;
                        $List->save();
                        $item = $List->list_id;
                    }
                    $meetHasList = new MeetingHasListMeet();
                    $meetHasList->meet_id = $id;
                    $meetHasList->list_id = $item;
                    $meetHasList->save();
                }
                // var_dump(Student::findSkillId($item,$id));
            }
        }
    }

    /**
     * Updates an existing Meeting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsFatlist = $model->fatLists;
        $modelsBotoxlist = $model->botoxLists;
        $customer = Yii::$app->request->get('customer');
        $name = Customer::findOne($customer)->customer_name;
        $listModel = Meeting::find()->where(['customer_id' => $customer])->one(); //หานักศึกษา

        if ($listModel->load(Yii::$app->request->post())) {
            try {
                $this->deleteCasecade($id);
            } catch (Exception $e) {
                echo $e;
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->status==0){
                $active = "edit";
            }else{
                $active = "";
            }
          /*  print_r($model->file);
            exit();*/
            $oldIDs = ArrayHelper::map($modelsFatlist, 'id', 'id');
            $modelsFatlist = ModelT::createMultiple(FatList::classname(), $modelsFatlist);
            ModelT::loadMultiple($modelsFatlist, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFatlist, 'id', 'id')));


            $oldIDs2 = ArrayHelper::map($modelsBotoxlist, 'id', 'id');
            $modelsBotoxlist = ModelT::createMultiple(BotoxList::classname(), $modelsBotoxlist);
            ModelT::loadMultiple($modelsBotoxlist, Yii::$app->request->post());
            $deletedIDs2 = array_diff($oldIDs2, array_filter(ArrayHelper::map($modelsBotoxlist, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = ModelT::validateMultiple($modelsFatlist) && $valid;

            $valid2 = $model->validate();
            $valid2 = ModelT::validateMultiple($modelsBotoxlist) && $valid2;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            FatList::deleteAll(['id' => $deletedIDs]);
                        }
                        if (!empty($deletedIDs2)) {
                            BotoxList::deleteAll(['id' => $deletedIDs2]);
                        }
                        foreach ($modelsFatlist as $modelsFatlist) {
                            $modelsFatlist->meet_id = $model->meet_id;
                            if (!($flag = $modelsFatlist->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelsBotoxlist as $modelsBotoxlist) {
                            $modelsBotoxlist->meet_id = $model->meet_id;
                            if (!($flag = $modelsBotoxlist->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    } else {
                        echo '<h1>กรุณาติดต่อ Programmer 0959957515 $KATE</h1><br/>';
                        var_dump($model->errors);
                        exit();
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->getSession()->setFlash('alert1', [
                            'type' => 'success',
                            'duration' => 12000,
                            'icon' => 'fas fa-check-circle',
                            'title' => Yii::t('app', Html::encode('Success')),
                            'message' => Yii::t('app', Html::encode('บันทึกสำเร็จ')),
                            'positonY' => 'top',
                            'positonX' => 'right'
                        ]);
                        return $this->redirect(['meet/' . $model->meet_id, 'customer' => $customer,'status'=>$active]);
                    }
                } catch (Exception $e) {
                    Yii::$app->getSession()->setFlash('alert1', [
                        'type' => 'warning',
                        'duration' => 12000,
                        'icon' => 'fas fa-exclamation-triangle',
                        'title' => Yii::t('app', Html::encode('Warning')),
                        'message' => Yii::t('app', Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                        'positonY' => 'top',
                        'positonX' => 'right'
                    ]);

                    $transaction->rollBack();
                    /*  IT WORK var_dump($e);
                      exit();*/
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'customer' => $customer,
            'name' => $name,
            'modelsFatlist' => (empty($modelsFatlist)) ? [new FatList] : $modelsFatlist,
            'modelsBotoxlist' => (empty($modelsBotoxlist)) ? [new BotoxList] : $modelsBotoxlist,
        ]);
    }

    /**
     * Deletes an existing Meeting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
        BotoxList::deleteAll(['meet_id' => $id]);
        FatList::deleteAll(['meet_id' => $id]);
        MeetingHasListMeet::deleteAll(['meet_id' => $id]);
        $this->findModel($id)->delete();
        $customerid = Yii::$app->request->get('customer');
            $transaction->commit();
            Yii::$app->getSession()->setFlash('alert1', [
                'type' => 'success',
                'duration' => 10000,
                'icon' => 'fas fa-check-circle',
                'title' => Yii::t('app', Html::encode('Success')),
                'message' => Yii::t('app',Html::encode('ลบข้อมูลสำเร็จ')),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        return $this->redirect(['meet/index/' . $customerid]);
        }catch (Exception $exception){
            $transaction->rollBack();
            throw $exception;
        }
    }

    /**
     * Finds the Meeting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Meeting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCaseModify($id)
    {
        $this->render('create_mo',[

        ]);
    }
    protected function findModel($id)
    {
        if (($model = Meeting::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
