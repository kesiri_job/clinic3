<?php

namespace app\controllers;

use Yii;
use app\models\Doctor;
use app\models\DoctorSearch;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DoctorController implements the CRUD actions for Doctor model.
 */
class DoctorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                    // ...
                ],
            ],
        ];
    }

    /**
     * Lists all Doctor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DoctorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Doctor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Doctor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Doctor();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'fas fa-check-circle',
                    'title' => Yii::t('app', Html::encode('Success')),
                    'message' => Yii::t('app',Html::encode('บันทึกสำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }else{
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'warning',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app',Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Doctor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'fas fa-check-circle',
                    'title' => Yii::t('app', Html::encode('Success')),
                    'message' => Yii::t('app',Html::encode('บันทึกสำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }else{
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'warning',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app',Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Doctor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
        $this->findModel($id)->delete();
            $transaction->commit();
        Yii::$app->getSession()->setFlash('alert1', [
            'type' => 'success',
            'duration' => 10000,
            'icon' => 'fas fa-check-circle',
            'title' => Yii::t('app', Html::encode('Success')),
            'message' => Yii::t('app',Html::encode('ลบข้อมูลสำเร็จ')),
            'positonY' => 'top',
            'positonX' => 'right'
        ]);
        }catch (Exception $exception){
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('alert1', [
                'type' => 'warning',
                'duration' => 10000,
                'icon' => 'fas fa-exclamation-triangle',
                'title' => Yii::t('app', Html::encode('Warning')),
                'message' => Yii::t('app',Html::encode('ไม่สามารถลบข้อมูลได้')),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Doctor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Doctor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Doctor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
