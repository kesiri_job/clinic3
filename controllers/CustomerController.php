<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/24/2018
 * Time: 11:03 AM
 */

namespace app\controllers;


use app\models\BotoxList;
use app\models\Customer;
use app\models\CustomerSearch;
use app\models\FatList;
use app\models\ListMeet;
use app\models\Meeting;
use app\models\MeetingHasListMeet;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CustomerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-delete',
            ],
            'avatar-delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }
    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $fat = FatList::find()
            ->innerJoin(['meeting', 'fat_list.meet_id = meeting.meet_id'])
           ->andWhere(['customer_id'=>2])->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Customer();

        if ($model->load(Yii::$app->request->post()) ) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'fas fa-check-circle',
                    'title' => Yii::t('app', Html::encode('Success')),
                    'message' => Yii::t('app',Html::encode('บันทึกสำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }else{
                print_r($model->errors);
                exit();
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'warning',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app',Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->customer_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
           /* print_r($model->file);
            exit();*/
            if($model->save()){
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'fas fa-check-circle',
                    'title' => Yii::t('app', Html::encode('Success')),
                    'message' => Yii::t('app',Html::encode('บันทึกสำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }else{
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'warning',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app',Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->customer_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $Meeting = Meeting::find()->where(['customer_id'=>$id])->all();
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($Meeting) {
                foreach ($Meeting as $item) {
                    BotoxList::deleteAll(['meet_id' => $item->meet_id]);
                    FatList::deleteAll(['meet_id' => $item->meet_id]);
                    MeetingHasListMeet::deleteAll(['meet_id' => $item->meet_id]);
                }
            }
            Meeting::deleteAll(['customer_id' => $id]);
            $this->findModel($id)->delete();
            $transaction->commit();
            Yii::$app->getSession()->setFlash('alert1', [
                'type' => 'success',
                'duration' => 10000,
                'icon' => 'fas fa-check-circle',
                'title' => Yii::t('app', Html::encode('Success')),
                'message' => Yii::t('app',Html::encode('ลบข้อมูลสำเร็จ')),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
            return $this->redirect(['index']);
        }catch (Exception $exception){
            $transaction->rollBack();
            throw $exception;
        }
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



}