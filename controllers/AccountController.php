<?php

namespace app\controllers;

use app\models\Expend;
use app\models\ExpendSearch;
use Yii;
use app\models\Income;
use app\models\IncomeSearch;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IncomeController implements the CRUD actions for Income model.
 */
class AccountController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionExpend()
    {
        $searchModel = new ExpendSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('expend_index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionExpendCreate()
    {
        $model = new Expend();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'far fa-check-circle',
                    'title' => Yii::t('app', Html::encode('success')),
                    'message' => Yii::t('app', Html::encode('บันทึกสำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'warning',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app', Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['expend']);
        }

        return $this->renderAjax('expend_create', [
            'model' => $model,
        ]);
    }
    public function actionExpendView($id)
    {
        return $this->renderAjax('expend_view', [
            'model' => $this->findModelExpend($id),
        ]);
    }
    public function actionExpendUpdate($id)
    {
        $model = $this->findModelExpend($id);
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'far fa-check-circle',
                    'title' => Yii::t('app', Html::encode('success')),
                    'message' => Yii::t('app',Html::encode('บันทึกสำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }else{
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'warning',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app',Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['expend']);
        }

        return $this->renderAjax('expend_update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Expend model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionExpendDelete($id)
    {
        $this->findModelExpend($id)->delete();
        Yii::$app->getSession()->setFlash('alert1', [
            'type' => 'success',
            'duration' => 10000,
            'icon' => 'far fa-check-circle',
            'title' => Yii::t('app', Html::encode('success')),
            'message' => Yii::t('app',Html::encode('ลสำเร็จ')),
            'positonY' => 'top',
            'positonX' => 'right'
        ]);
        return $this->redirect(['expend']);
    }

    /**
     * Lists all Income models.
     * @return mixed
     */
    public function actionIncome()
    {
        $searchModel = new IncomeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('income', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Income model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIncomeView($id)
    {
        return $this->renderAjax('income_view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Income model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionIncomeCreate()
    {
        $model = new Income();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'far fa-check-circle',
                    'title' => Yii::t('app', Html::encode('success')),
                    'message' => Yii::t('app',Html::encode('บันทึกสำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }else{
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'warning',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app',Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['income']);
        }

        return $this->renderAjax('income_create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Income model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIncomeUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'success',
                    'duration' => 10000,
                    'icon' => 'far fa-check-circle',
                    'title' => Yii::t('app', Html::encode('success')),
                    'message' => Yii::t('app',Html::encode('บันทึกสำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }else{
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'warning',
                    'duration' => 10000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app',Html::encode('มีข้อผิดพลาด บันทึกไม่สำเร็จ')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['income']);
        }

        return $this->renderAjax('income_update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Income model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIncomeDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('alert1', [
            'type' => 'success',
            'duration' => 10000,
            'icon' => 'far fa-check-circle',
            'title' => Yii::t('app', Html::encode('success')),
            'message' => Yii::t('app',Html::encode('ลบข้อมูลสำเร็จ')),
            'positonY' => 'top',
            'positonX' => 'right'
        ]);
        return $this->redirect(['income']);
    }

    /**
     * Finds the Income model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Income the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Income::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findModelExpend($id)
    {
        if (($model = Expend::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
