<?php

namespace app\controllers;

use app\models\Appointment;
use app\models\Customer;
use app\models\Expend;
use app\models\File;
use app\models\FileStorageItem;
use app\models\Income;
use app\models\ListMeet;
use app\models\MeetingHasListMeet;
use app\models\Stock;
use yii\db\Exception;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use app\models\Tr14;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    /*    public function actions()
        {
            return [
                'error' => [
                    'class' => 'yii\web\ErrorAction',
                ],
                'captcha' => [
                    'class' => 'yii\captcha\CaptchaAction',
                    'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                ],
            ];
        }*/
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionCalender()
    {
        /*$events = array();
        //Testing
        $appointment = Appointment::find()->all();
        $Event = new \yii2fullcalendar\models\Event();
        $Event->id = 1;
        $Event->title = 'Testing';
        $Event->start = date('Y-m-d\Th:m:s\Z');
        $events[] = $Event;

        $Event = new \yii2fullcalendar\models\Event();
        $Event->id = 2;
        $Event->title = 'Testing';
        $Event->start = date('Y-m-d\Th:m:s\Z',strtotime('tomorrow 6am'));
        $events[] = $Event;*/
        return $this->render('calender',[
           // 'events'=>$events
        ]);
    }
    public function actionJsoncalendar($start=NULL,$end=NULL,$_=NULL){
        $appointment = Appointment::find()->all();
        $events = array();

        foreach ($appointment AS $key =>$time){
            //Testing
            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $time->appoint_id;
            $Event->title = $time->appoint_name;
            $Event->start = date('Y-m-d\Th:m:s\Z',strtotime($time->appoint_date));
            //$Event->end = date('Y-m-d\Th:m:s\Z',strtotime($time->appoint_date));
            $Event->url = \Yii::getAlias('@web').'/appoint/'.$time->appoint_id;
            $Event->color = '#3960d1';
            $events[] = $Event;
        }

        header('Content-type: application/json');
        echo Json::encode($events);

        Yii::$app->end();
    }
    public function actionAjaxCalendar()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $start = \Yii::$app->request->post('start');
        $end = \Yii::$app->request->post('end');
          $activity = Appointment::find()->all();


        $data = [];
        foreach ($activity as $key => $item) {
            if (strtotime($item->date_start) != strtotime($item->date_end)) {
                $end = strtotime("+1 day", strtotime($item->date_end));
                $item->date_end = date("Y-m-d", $end);
            }
            if($item->approve == 1) $bgColor = '#2eb398';
            elseif($item->approve == 2) $bgColor = '#FF6C60';
            elseif($item->approve == 3) $bgColor = '#3f51b5';
            elseif($item->approve == 4) $bgColor = '#ebc142';

            $data[$key]['start'] = $item->date_start;
            $data[$key]['end'] = $item->date_end;
            $data[$key]['title'] = $item->activity_name_th;
            $data[$key]['color'] = $bgColor;
            $data[$key]['url'] = \Yii::getAlias('@web').'/activity-user/my-view?id='.$item->id;
        }
        return $data;
    }
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $user = Customer::find()->groupBy(['customer_id'])->count();

        $ym1 = Yii::$app->formatter->asDate('now', 'yyyy-MM');
        $ymd1 = $ym1."-1"; //เอาแต่วันที่ 1 ของเดือนนี้

        $currentDate = date('Y-m-d');
        $ymd2 = date("Y-m-t", strtotime($currentDate));


        $query = "SELECT
                    Count(meeting_has_list_meet.list_id) AS count,
                    list_meet.list_name
                    FROM
                    meeting_has_list_meet
                    INNER JOIN list_meet ON meeting_has_list_meet.list_id = list_meet.list_id
                    INNER JOIN meeting ON meeting_has_list_meet.meet_id = meeting.meet_id
                    WHERE
                    meeting.meet_date BETWEEN '".$ymd1."' AND '".$ymd2."'
                    GROUP BY
                    meeting_has_list_meet.list_id
                    LIMIT 5";

        $commandCountSt = Yii::$app->db->createCommand($query)->queryAll();
        $main_items = [];
        foreach ($commandCountSt AS $data) {
            $main_items[] = [
                'count' => $data['count'],
                'name' => $data['list_name'],
            ];
        }

        $stock = Stock::find()->where(['status'=>'1'])->all();
        $pill = [];

        if($stock){
            foreach ($stock AS $key=>$data) {
                if($data->left<=5){
                    $pill[$key]["name"] = $data->stock_name;
                    $pill[$key]["id"] = $data->stock_id;
                    $pill[$key]["left"] = $data->left;
                }
            }
        }
      /*  echo $main_items[0]['count'];
        exit();*/

        //Appointment
        $currentDate = date('Y-m-d');
        $appoint = Appointment::find()->filterWhere(['like', 'appoint_date', $currentDate])->andWhere(['status'=>'0'])->all();

        $income = Income::find()->where( ["between",".income_date", $ymd1, $ymd2] )->sum('income_number');
        $expend = Expend::find()->where( ["between",".expend_date", $ymd1, $ymd2] )->sum('expend_number');
     /*   var_dump($income);
        exit();*/

        if (Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(['user/login']);
        } else {
            // return Yii::$app->getResponse()->redirect( ['location/index'] );
            return $this->render('index', [
                'main_items' => $main_items ? $main_items : null,
                'customer'=> empty($user) ? 0 : $user,
                'pill'=>$pill?$pill:null,
                'appoint'=>$appoint?$appoint:null,
                'income'=>$income?$income:0,
                'expend'=>$expend?$expend:0,
            ]);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }
}
