<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/14/2018
 * Time: 5:58 PM
 */

namespace app\controllers\user;
use app\models\User;
use app\models\Userabt;
use dektrium\user\controllers\RegistrationController as BaseAdminController;
use dektrium\user\models\RegistrationForm;
use yii\web\NotFoundHttpException;

class RegistrationController extends BaseAdminController
{

    public function actionRegister()
    {
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }

        /** @var RegistrationForm $model */
        $model = \Yii::createObject(RegistrationForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_REGISTER, $event);

        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->request->post()) && $model->register()) {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);

            return $this->render('/message', [
                'title'  => \Yii::t('user', 'Your account has been created'),
                'module' => $this->module,
            ]);
        }

        return $this->render('register', [
            'model'  => $model,
            'module' => $this->module,
        ]);
    }

}