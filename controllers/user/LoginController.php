<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/29/2018
 * Time: 11:07 PM
 */

namespace app\controllers\user;
use app\models\Userabt;
use dektrium\user\controllers\SecurityController as BaseAdminController;
use dektrium\user\models\LoginForm;
use Yii;
use yii\helpers\Html;

class LoginController extends BaseAdminController
{
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            $this->goHome();
        }

        /** @var LoginForm $model */
        $model = \Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->login()) {
            $user = Userabt::findOne(Yii::$app->user->identity->id);
            if($user->status=="0"){
                \Yii::$app->getUser()->logout();
                Yii::$app->getSession()->setFlash('alert1', [
                    'type' => 'warning',
                    'duration' => 12000,
                    'icon' => 'fas fa-exclamation-triangle',
                    'title' => Yii::t('app', Html::encode('Warning')),
                    'message' => Yii::t('app',Html::encode('ขออภัยบัญชีของคุณยังไม่ถูกเปิดใช้งาน')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);

            }
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->goBack();
        }
        return parent::actionLogin();
    }


}