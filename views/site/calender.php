<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/23/2018
 * Time: 10:36 PM
 */
?>
<div class="portlet">
    <div class="portlet-heading">
        <h3 class="portlet-title text-dark">
            Calender (ปฎิทิน)
        </h3>
        <div class="portlet-widgets">
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="bg-default" class="panel-collapse collapse in">
        <div class="portlet-body">
            <?= yii2fullcalendar\yii2fullcalendar::widget(array(
                'options' => [
                    'lang' => 'th',
                ],
                'ajaxEvents' => \yii\helpers\Url::to(['/site/jsoncalendar'])
            ));
            ?>
        </div>
    </div>
</div>
