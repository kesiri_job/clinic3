<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/7/2018
 * Time: 2:16 PM
 */
use miloschuman\highcharts\Highcharts;

?>
<div class="row">
    <div class="col-md-4">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon bg-pink"><i class="fa fa-user"></i></span>
            <div class="mini-stat-info text-right">
                <span class="counter"><?= $customer ?></span>
                Customer In System
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon bg-info"><i class="fas fa-dollar-sign"></i></span>
            <div class="mini-stat-info text-right">
                <span class="counter"><?=$income?></span>
                Total Income In Month
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon bg-warning"><i class="fa fa-shopping-cart"></i></span>
            <div class="mini-stat-info text-right">
                <span class="counter"><?=$expend?></span>
                Total Expenditure In Month
            </div>
        </div>
    </div>
</div>
<div class="portlet">
    <div class="portlet-heading ">
        <div class="pull-left">
            <h2 class="portlet-title text-dark">
                Notification
            </h2>
        </div>
        <div class="pull-right">
            <a href="<?=Yii::getAlias('@web/site/calender')?>" class="btn btn-pink btn-rounded m-b-5"><i class="fas fa-calendar-alt"></i> ปฎิทิน</a>
        </div>
<br/>
    </div>
    <div id="bg-primary" class="panel-collapse collapse in">
        <div class="portlet-body">
            <div class="row">

            <?php if($pill){
                foreach ($pill as $data){?>
                <div class="col-md-6">
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span style="font-size:20px"> <i class="fas fa-pills"></i> </span>
                    <a href="<?= Yii::getAlias('@web/stock/update/').$data["id"] ?>" class="alert-link"> แจ้งเตือน <?= $data["name"] ?> เหลือ <?= $data["left"] ?></a>
                </div>
                </div>
            <?php }}?>


            <?php if($appoint){
                foreach ($appoint as $data){?>
            <div class="col-md-6">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <span style="font-size:20px;color: #b94a48"> <i class="far fa-comments"></i> </span>
                        <a href="<?= Yii::getAlias('@web/appoint/').$data["appoint_id"] ?>" class="alert-link">มีการนัดพบวันนี้เรื่อง <?=$data["appoint_name"]?> กับคุณ  <?=$data->customer->customer_name?> </a>
                    </div>
            </div>
                    <?php
                }
            }?>
                <?php if(!$appoint&&!$pill){ ?>
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <span style="font-size:20px"> <i class="far fa-bell"></i> </span>
                        <a href="#" class="alert-link"> No Notification</a>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>
<div class="portlet">
    <div class="portlet-heading ">
        <h2 class="portlet-title text-dark">
            Graph
        </h2>
    </div>
    <div id="bg-primary" class="panel-collapse collapse in">
        <div class="portlet-body">
            <?php
            echo Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],

                'options' => [
                    'title' => [
                        // 'text' => 'กราฟแสดงรายการสูงสุด 5 อันดับแรก',
                    ],
                    'xAxis' => [
                        'classname' => 'highcharts-color-0',
                        'categories' => ['รายการ'],
                    ],
                    'credits' => ['enabled' => false],
//                            'colors'=> ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572',
//                                '#FF9655', '#FFF263', '#6AF9C4'],
                    'labels' => [
                        'items' => [
                            [
                                'style' => [
                                    'left' => '50px',
                                    'top' => '18px',
                                    'color' => new \yii\web\JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                ],
                            ],
                        ],
                    ],
                    'series' => [

                        [
                            'type' => 'column',
                            'name' => empty($main_items[0]['name']) ? null : $main_items[0]['name'],
                            'data' => [empty($main_items[0]['count']) ? null : (int)$main_items[0]['count'],],
                        ],
                        [
                            'type' => 'column',
                            'name' => empty($main_items[1]['name']) ? null : $main_items[1]['name'],
                            'data' => [empty($main_items[1]['count']) ? null : (int)$main_items[1]['count'],],
                        ],
                        [
                            'type' => 'column',
                            'name' => empty($main_items[2]['name']) ? null : $main_items[2]['name'],
                            'data' => [empty($main_items[2]['count']) ? null : (int)$main_items[2]['count'],],
                        ],
                        [
                            'type' => 'column',
                            'name' => empty($main_items[3]['name']) ? null : $main_items[3]['name'],
                            'data' => [empty($main_items[3]['count']) ? null : (int)$main_items[3]['count'],],
                        ],
                        [
                            'type' => 'column',
                            'name' => empty($main_items[4]['name']) ? null : $main_items[4]['name'],
                            'data' => [empty($main_items[4]['count']) ? null : (int)$main_items[4]['count'],],
                        ],


                    ],
                ]
            ]);
            ?>
        </div>
    </div>
</div>


