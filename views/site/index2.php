<?php

/* @var $this yii\web\View */

use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

$this->title = 'Cher Charm Clinic';
?>

<div class="row">
    <div class="col-sm-4" align="left">
        <!--  <div class="panel bg-primary">
              <div class="panel-body">
                  <div class="h2 text-white m-t-10">956</div>
                  <p class="text-white m-b-10">New Customer</p>
              </div>
          </div>
        <!-- <div class="panel panel-default">
        <div class="panel-body">
            <h2> <i class="far fa-calendar-alt" style="font-size: 48px;"></i>
                &nbsp; <? /*= \app\controllers\GetpublicController::getDateThaiTime(date('Y-m-d')) */ ?></h2>
        </div>
    </div>-->
        <div class="list-group">
            <div class="list-group-item bg-primary">
                <div class="row">
                    <div class=" col-md-12" align="left">
                        <div style="font-size: 25px"><b><?= $customer ?></b></div>
                        <span style="font-size: 15px">Customer In System</span>
                    </div>
                </div>
            </div>
            <a href="<?= Yii::getAlias('@web') ?>/stock/index" class="list-group-item">
                <div class="row">
                    <div class="col-xs-2 col-md-2" align="left">
                        <span style="color:#007aff;font-size: 25px"><i class="fas fa-pills"></i></span>
                    </div>
                    <div class="col-xs-2 col-md-1" align="left">
                        <span style="font-size: 25px">Stock</span>
                    </div>
                </div>
            </a>
            <a href="<?= Yii::getAlias('@web') ?>/customer/index" class="list-group-item">
                <div class="row">
                    <div class="col-xs-2 col-md-2" align="left">
                        <span align="left" style="color:#007aff;font-size: 25px"><i class="fas fa-users"></i></span>
                    </div>
                    <div class="col-xs-2 col-md-1" align="left">
                        <span align="center" style="font-size: 25px">Customer</span>
                    </div>
                </div>
            </a>
            <a href="<?= Yii::getAlias('@web') ?>/appoint/index" class="list-group-item">
                <div class="row">
                    <div class="col-xs-2 col-md-2" align="left">
            <span style="color:#007aff;font-size: 25px">
                <i class="far fa-comments"></i></span>
                    </div>
                    <div class="col-xs-4 col-md-1" align="left">
                        <span style="font-size: 25px">Appointment</span>
                    </div>
                    <div class="col-xs-6 col-md-9" align="right">
                        <span class="label label-danger">2</span>
                    </div>
                </div>

            </a>
            <a href="<?= Yii::getAlias('@web') ?>/doctor/index" class="list-group-item">
                <div class="row">
                    <div class="col-xs-2 col-md-2" align="left">
            <span style="color:#007aff;font-size: 25px">
                <i class="fas fa-user-md"></i>
            </span>
                    </div>
                    <div class="col-xs-6 col-md-6" align="left">
                        <span style="font-size: 25px">Add Doctor</span>
                    </div>
                </div>
            </a>
            <a href="<?= Yii::getAlias('@web') ?>/users/approve" class="list-group-item">
                <div class="row">
                    <div class="col-xs-2 col-md-2" align="left">
            <span style="color:#007aff;font-size: 25px">
                <i class="fas fa-users-cog"></i>
            </span>
                    </div>
                    <div class="col-xs-6 col-md-6" align="left">
                        <span style="font-size: 25px">Approve User</span>
                    </div>
                </div>
            </a>
            <a class="list-group-item" data-toggle="modal" data-target="#myModal">
                <div class="row">
                    <div class="col-xs-2 col-md-2" align="left">
                        <span style="color:#007aff;font-size: 25px"><i class="far fa-file-alt"></i></span>
                    </div>
                    <div class="col-xs-2 col-md-1" align="left">
                        <span style="font-size: 25px">Report</span>
                    </div>
                </div>
            </a>
            <a href="<?= Yii::getAlias('@web') ?>/site/graph" class="list-group-item" >
                <div class="row">
                    <div class="col-xs-2 col-md-2" align="left">
            <span style="color:#007aff;font-size: 25px">
                <i class="fas fa-chart-line"></i>
            </span>
                    </div>
                    <div class="col-xs-2 col-md-1" align="left">
                        <span style="font-size: 25px">Graph</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-sm-8">


    </div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <?php $form = ActiveForm::begin(['action' => 'graph/index', 'id' => 'report',]); ?>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label" for="from">จาก</label>
                        <?= \dosamigos\datepicker\DatePicker::widget([
                            'name' => 'from',
                            'id' => 'from',
                            'value' => date('Y-m-d'),
                            'template' => '{addon}{input}',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]); ?>

                    </div>
                    <div class="col-md-6">
                        <label class="control-label" for="to">ถึง</label>
                        <?= \dosamigos\datepicker\DatePicker::widget([
                            'name' => 'to',
                            'value' => date('Y-m-d'),
                            'id' => 'to',
                            'template' => '{addon}{input}',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" align="center">
                        <br/>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary w-md m-b-5" onclick="download('list-report')">
                    <i class="fas fa-file-excel"></i> Excel
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>