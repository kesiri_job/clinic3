<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;


?>
<br/><br/>
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?php Html::hiddenInput('txt_excel_path', '', ['class' => 'form-control', 'readonly' => true, 'id' => 'txt_excel_path']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php $form = ActiveForm::begin(['id'=>'import','options' => ['enctype'=>'multipart/form-data']]); ?>
            <?= $form->field($model,'file')->fileInput([])->label('Select File') ?>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <?php echo Html::submitButton('<i class="fa fa-save"></i> บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>