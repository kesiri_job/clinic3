<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Userabt */

$this->params['breadcrumbs'][] = ['label' => 'Approve User', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="userabt-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
