<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Userabt */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Userabts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->username;
?>
<div class="userabt-view">
    <div class="portlet">
        <div class="portlet-heading ">
            <h2 class="portlet-title text-dark">
               ดูข้อมูล
            </h2>
        </div>
        <div id="bg-primary" class="panel-collapse collapse in">
            <div class="portlet-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email:email',
            'created_at',
            'updated_at',
            'last_login_at',
            [
                'label' => 'status',
                'value' => empty($model->status)? "Not Active" :"Active",
            ],
        ],
    ]) ?>


            </div>
        </div>
    </div>
</div>
