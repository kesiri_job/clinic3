<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = $model->customer_id;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->customer_name;
?>
<div class="customer-view">
    <div class="col-lg-12">
        <div class="portlet">
            <div id="bg-primary" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-2">
                            <br/>
                           <img src="<?= $model->getPicture() ?>" width="150" class="img-thumbnail">
                        </div>
                        <div class="col-md-10">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th colspan="3"><h4>ดูข้อมูล</h4></th>
                                        <th>
                                            <div align="right">
                                                <?= Html::a('<i class="fas fa-edit"></i> Update', ['update?id=' . $model->customer_id], ['class' => 'btn btn-success']) ?>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><b>ชื่อ-นามสกุล</b></td>
                                        <td><?= $model->customer_name ?></td>
                                        <td><b>เพศ</b></td>
                                        <td><?= $model->gender ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>วันเกิด</b></td>
                                        <td><?= \app\controllers\GetpublicController::getDateThaiTime($model->birth_dath) ?></td>
                                        <td><b>ชื่อเล่น</b></td>
                                        <td><?= $model->nicname ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>เบอร์โทร</b></td>
                                        <td><?= $model->phone ?></td>
                                        <td><b>อายุ</b></td>
                                        <td><?= $model->age ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>น้ำหนัก</b></td>
                                        <td><?= $model->weight ?></td>
                                        <td><b>ส่วนสูง</b></td>
                                        <td><?= $model->height ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>อาชีพ</b></td>
                                        <td colspan="3"><?= $model->career ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>โรคประจำตัว</b></td>
                                        <td colspan="3"><?= $model->disease ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>ประวัติการแพ้ยา</b></td>
                                        <td colspan="3"><?= $model->history_allergy ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



