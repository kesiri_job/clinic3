<?php

use app\models\Customer;
use app\models\FatList;
use app\models\ListMeet;
use app\models\Meeting;
use app\models\Profile;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">
    <div class="col-lg-12">
        <div class="portlet">
            <div class="portlet-heading ">
                <h2 class="portlet-title text-dark">
                    <?= Html::encode($this->title) ?>
                </h2>
                <div class="portlet-widgets">
                    <?= Html::a('<i class="zmdi zmdi-collection-plus"></i> Create Customer', ['create?active=0'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <div id="bg-primary" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-10">
                            <?= $this->render('_search', ['model' => $searchModel]); ?>
                        </div>
                        <div class="col-md-2">
                            <br/>
                            <div align="right">
                                <?php
                                $gridColumns = [
                                    ['class' => 'kartik\grid\SerialColumn'],
                                    'id_card',
                                    'customer_name',
                                    'nicname',
                                    'phone',
                                    'gender',
                                    'career',
                                    'age',
                                    'height',
                                    'weight',
                                    'disease',
                                    'history_allergy',
                                    'birth_dath',
                                    /*  ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
                                          return '#';
                                      }]*/
                                ];

                                echo ExportMenu::widget([
                                    'dataProvider' => $dataProvider,
                                    'columns' => $gridColumns,
                                    'fontAwesome' => true,
                                    'target' => '_blank',
                                    'exportConfig' => [
                                        ExportMenu::FORMAT_TEXT => false,
                                        ExportMenu::FORMAT_PDF => false,
                                        ExportMenu::FORMAT_CSV => false,
                                    ],
                                ]);


                                ?>
                            </div>
                        </div>
                    </div>
                        <br/>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'responsiveWrap' => false,
                            'layout' => '{items}{summary}{pager}',
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'id_card',
                                'customer_name',
                                'phone',
                                [
                                    'hAlign' => 'center',
                                    'header' => 'การรักษา',
                                    'options' => ['style' => 'width:250px;'],
                                    'format' => 'html',
                                    'value' => function ($model, $key, $index, $column) {
                                        $list = "SELECT
                                            list_meet.list_name,
                                            list_meet.list_id
                                            FROM `list_meet` 
                                            LEFT JOIN meeting_has_list_meet ON meeting_has_list_meet.list_id = list_meet.list_id
                                            LEFT JOIN meeting ON meeting_has_list_meet.meet_id = meeting.meet_id
                                            LEFT JOIN customer ON meeting.customer_id = customer.customer_id
                                            WHERE customer.customer_id=" . $model->customer_id . "
                                            GROUP BY
                                            list_meet.list_name";
                                        $fat = "SELECT
                                            fat_list.fat_cc
                                            FROM
                                            fat_list
                                            INNER JOIN meeting ON fat_list.meet_id = meeting.meet_id
                                            WHERE
                                            meeting.customer_id =" . $model->customer_id;
                                        $botox = "SELECT
                                            botox_list.botox_u
                                            FROM
                                            botox_list
                                            INNER JOIN meeting ON botox_list.meet_id = meeting.meet_id
                                            WHERE
                                            meeting.customer_id =" . $model->customer_id;
                                        $icon = "";
                                        $commandFat = Yii::$app->db->createCommand($fat)->queryAll();
                                        $commandCountSt = Yii::$app->db->createCommand($list)->queryAll();
                                        $commandBotox = Yii::$app->db->createCommand($botox)->queryAll();
                                        if ($commandFat) {
                                            foreach ($commandFat as $value) {
                                                if ($value["fat_cc"] != null) {
                                                    $icon .= " <span class='label label-default'>Fat</span> ";
                                                    break;
                                                }
                                            }
                                        }
                                        if ($commandBotox) {
                                            foreach ($commandBotox as $value) {
                                                if ($value["botox_u"] != null) {
                                                    $icon .= " <span class='label label-warning'>Botox</span> ";
                                                    break;
                                                }
                                            }
                                        }
                                        if ($list) {
                                            foreach ($commandCountSt as $key => $value) {
                                                if ($value["list_id"] == 1) {
                                                    $icon .= " <span class='label label-primary'>" . $value["list_name"] . "</span> ";
                                                }
                                                if ($value["list_id"] == 2) {
                                                    $icon .= " <span class='label label-pink'>" . $value["list_name"] . "</span> ";
                                                }
                                                if ($value["list_id"] == 3) {
                                                    $icon .= " <span class='label label-success'>" . $value["list_name"] . "</span> ";
                                                }
                                                if ($value["list_id"] == 4) {
                                                    $icon .= " <span class='label label-info'>" . $value["list_name"] . "</span> ";
                                                }
                                                if ($value["list_id"] == 5) {
                                                    $icon .= " <span class='label label-purple'>" . $value["list_name"] . "</span> ";
                                                }
                                            }
                                        }


                                        return $icon;
                                    }
                                ],
                                [
                                    'header' => 'เพิ่มการเรักษา',
                                    'hAlign' => 'center',
                                    'options' => ['style' => 'width:120px;'],
                                    'format' => 'html',
                                    'value' => function ($model, $key, $index, $column) {
                                        return Html::a('<i class="fas fa-plus"></i> Add Meet', ['meet/index', 'id' => $model->customer_id], [
                                            'class' => 'btn btn-block btn-primary btn-xs',
                                            'clientOptions' => ['backdrop' => false]
                                        ]);
                                    }
                                ],

                                ['class' => 'kartik\grid\ActionColumn',
                                    'options' => ['style' => 'width:130px;'],
                                    'buttonOptions' => ['class' => 'btn btn-default'],
                                    'template' => '<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} {delete} </div>',

                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>