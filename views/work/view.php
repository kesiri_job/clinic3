<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Work */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Works', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-view">
    <div class="portlet">
        <div id="bg-primary" class="panel-collapse collapse in">
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'label' => 'ชื่อ',
                            'value' => \app\models\Profile::findOne($model->user_id)->name,
                        ],
                        [
                            'label' => 'รายการ',
                            'value' => empty($model->typeName) ? null : $model->typeName,
                        ],
                        [
                            'label' => 'ประเภทบุคคล',
                            'value' => empty($model->type2Name) ? null : $model->type2Name,
                        ],
                        [
                            'label' => 'วันที่เข้างาน',
                            'value' => \app\controllers\GetpublicController::getDateThaiTime($model->work_date),
                        ],
                        'work_time_start:time',
                        'work_time_end:time',
                        /* [
                             'format' => 'html',
                             'label' => 'จำนวนชั่วโมง',
                             'value' => function($model){
                                 $h = $model->work_time_end - $model->work_time_start;
                                 return $h;
                             },
                         ],*/
                        [
                            'format' => 'html',
                            'label' => 'หมายเหตุ',
                            'value' => empty($model->work_detail) ? null : $model->work_detail,
                        ],

                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>
