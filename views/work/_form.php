<?php

use kartik\select2\Select2;
use kartik\widgets\TimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Work */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-form">
    <div class="portlet">
        <div class="portlet-heading ">
            <h2 class="portlet-title text-dark">
                <?= empty($_GET['active']) ? "เพิ่มข้อมูล" : "แก้ไขข้อมูล" ?>
            </h2>
        </div>
        <div id="bg-primary" class="panel-collapse collapse in">
            <div class="portlet-body">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'work_date')->widget(\kartik\widgets\DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter date ...'],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])->label('วันที่'); ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'work_time_start')->widget(TimePicker::classname(), [
                            'pluginOptions' => [
                                'showSeconds' => true,
                                'showMeridian' => false,
                                'minuteStep' => 1,
                                'secondStep' => 5,
                            ]
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'work_time_end')->widget(TimePicker::classname(), [
                            'pluginOptions' => [
                                'showSeconds' => true,
                                'showMeridian' => false,
                                'minuteStep' => 1,
                                'secondStep' => 5,
                            ]
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Userabt::find()->innerJoin(['profile', 'user.id = profile.user_id'])->where(['status' => '1'])->all(), 'profile.user_id', 'profile.name'),
                            'options' => ['placeholder' => 'เลือกรายชื่อ'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('ชื่อ'); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'work_type')
                            ->widget(Select2::classname(), [
                                'data' => ['1' => 'เข้างาน', '0' => 'ลางาน'],
                                'options' => ['placeholder' => 'เลือกรายการ'],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ],
                            ])->label('รายการ'); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'work_type2')
                            ->widget(Select2::classname(), [
                                'data' => ['1' => 'พนักงาน', '2' => 'หมอ'],
                                'options' => ['placeholder' => 'เลือกประเภทบุคคล'],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ],
                            ]); ?>
                    </div>
                </div>


                <?php echo $form->field($model, 'work_detail')->widget(\dosamigos\tinymce\TinyMce::className(), [
                    'options' => ['rows' => 3],
                    'language' => 'es',
                    'clientOptions' => [
                        'plugins' => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste"
                        ],
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    ]
                ]); ?>
                <div class="form-group" align="right">
                    <?= Html::submitButton('<i class="fa fa-save"></i>  Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
