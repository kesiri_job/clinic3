<?php

use app\controllers\GetpublicController;
use app\models\Profile;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Works';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$this->registerJs('
        function init_click_handlers(){
            $("#activity-create-link").click(function(e) {
                    $.get(
                        "create",
                        function (data)
                        {                        
                            $("#activity-modal").find(".modal-body").html(data);    
                            $(".modal-body").html(data);
                              tinymce.remove();
                              tinymce.init({selector: "textarea"});
                             $(".modal-title").html("เพิ่มข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });
            $(".activity-view-link").click(function(e) {
                    var fID = $(this).closest("tr").data("key");
                    $.get(
                        "view",
                        {
                            id: fID
                        },
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                            $(".modal-title").html("เปิดดูข้อมูล");
                            $("#activity-modal").modal("show");
                            
                        }
                    );
                });
            $(".activity-update-link").click(function(e) {
                    var fID = $(this).closest("tr").data("key");
                    $.get(
                        "update",
                        {
                            id: fID
                        },
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                              tinymce.remove();
                              tinymce.init({selector: "textarea"});
                            $(".modal-title").html("ปรับปรุงข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });     
        }
        init_click_handlers(); //first run
        $("#customer_pjax_id").on("pjax:success", function() {
       
          init_click_handlers(); //reactivate links in grid after pjax update
         
        });'); ?>
    <div class="work-index">
        <div class="portlet">
            <div class="portlet-heading ">
                <h2 class="portlet-title text-dark">
                    <?= Html::encode($this->title) ?>
                </h2>
                <div class="portlet-widgets">
                    <?= Html::a('<i class="zmdi zmdi-collection-plus"></i> Create Work', ['create'],['class' => 'btn btn-success', 'id' => 'activity-create-link']); ?>
                </div>
                <?php \yii\bootstrap\Modal::begin([
                    'id' => 'activity-modal',
                    'header' => '<h4 class="modal-title"></h4>',
                    'size' => 'modal-lg',
                    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">ปิด</a>',
                ]);
                \yii\bootstrap\Modal::end();
                ?>
            </div>
            <div id="bg-primary" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-10">
                            <?php // $this->render('_search', ['model' => $searchModel]); ?>
                        </div>
                        <div class="col-md-2">
                            <br/>
                            <div align="right">
                                <?php
                                $gridColumns = [
                                    ['class' => 'kartik\grid\SerialColumn'],
                                    [
                                        'attribute' => 'user_id',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            $user = Profile::findOne($model->user_id);
                                            return $user ? $user->name : null;
                                        },
                                    ],
                                    [
                                        'attribute' => 'work_type',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->getTypeName();
                                        },
                                    ],
                                    [
                                        'attribute' => 'work_type2',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->getType2Name();
                                        },
                                    ],
                                    [
                                        'attribute' => 'work_date',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->work_date ? $model->work_date : null;
                                        },
                                        'format' => 'date'
                                    ],
                                    [
                                        'attribute' => 'work_time_start',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->work_time_start ? $model->work_time_start : null;
                                        },
                                        'format' => 'time'
                                    ],
                                    [
                                        'attribute' => 'work_time_end',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->work_time_end ? $model->work_time_end : null;
                                        },
                                        'format' => 'time'
                                    ],
                                    [
                                        'attribute' => 'work_detail',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->work_detail ? $model->work_detail : null;
                                        },
                                        'format' => 'html'
                                    ],
                                    /*  ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
                                          return '#';
                                      }]*/
                                ];

                                echo ExportMenu::widget([
                                    'dataProvider' => $dataProvider,
                                    'columns' => $gridColumns,
                                    'fontAwesome' => true,
                                    'target' => '_blank',
                                    'exportConfig' => [
                                        ExportMenu::FORMAT_TEXT => false,
                                        ExportMenu::FORMAT_PDF => false,
                                        ExportMenu::FORMAT_CSV => false,
                                    ],
                                ]);


                                ?>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <?php //Pjax::begin(['id' => 'pjax_99','timeout' => 5000]); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'responsiveWrap' => false,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'kartik\grid\SerialColumn'],
                            [
                                'attribute' => 'user_id',
                                'value' => function ($data) {
                                    $user = Profile::findOne($data->user_id);
                                    return $user?$user->name:null;
                                }
                            ],
                            [
                                'attribute'=> 'work_date',
                                //'format' => 'date',
                                'value' => function ($data) {
                                    return GetpublicController::getDateThaiTime($data->work_date);
                                },
                                'filter' => \kartik\widgets\DatePicker::widget([
                                    'attribute' => 'work_date',
                                    'model'=>$searchModel,
                                    'name' => 'dp_1',
                                    'type' => \kartik\widgets\DatePicker::TYPE_INPUT,
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'yyyy-mm-dd'
                                    ]
                                ]),
                            ],
                          //  'work_list',
                            [
                                'hAlign' => 'center',
                                'label' => 'รายการ',
                                'attribute' => 'work_type',
                                'format' => 'html',
                                'filter' => $searchModel->itemType,
                                'value' => function ($model, $key, $index, $column) {
                                    return $model->work_type == 1 ? "<span class='label label-primary'>เข้างาน</span>" : "<span class='label label-default'>ลางาน</span>";
                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => ['placeholder' => 'สถานะ'],
                            ],

                            [
                                'hAlign' => 'center',
                                'label' => 'ประเภทบุคคล',
                                'attribute' => 'work_type2',
                                'format' => 'html',
                                'filter' => $searchModel->itemType2,
                                'value' => function ($model, $key, $index, $column) {
                                    return $model->work_type2 == 1 ? "<span class='label label-pink'>พนักงาน</span>" : "<span class='label label-purple'>หมอ</span>";
                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => ['placeholder' => 'สถานะ'],
                            ],


                            ['class' => 'kartik\grid\ActionColumn',
                                'options' => ['style' => 'width:140px;'],
                                'buttonOptions' => ['class' => 'btn btn-default'],
                                'template' => '<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} {delete} </div>',
                                'buttons' => [
                                    'view' => function ($url, $model, $key) {
                                        return Html::a('<i class="far fa-eye"></i>', ['view', 'id' => $model->id], [
                                            'class' => 'btn btn-default',
                                            'title' => 'View',
                                      /*      'data-toggle' => 'modal',
                                            'data-target' => '#activity-modal',
                                            'data-id' => $key,
                                            'data-pjax' => '0',*/
                                        ]);
                                    },
                                    'update' => function ($url, $model, $key) {
                                        return Html::a('<i class="fas fa-pencil-alt"></i>', ['update', 'id' => $model->id,'active'=>'1'], [
                                            'class' => 'btn btn-default',
                                            'title' => 'Update',
                                        ]);
                                    },
                                    'delete' => function ($url, $model, $key) {
                                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id],
                                            ['data-method' => 'post',
                                                'data-confirm' => 'Are you sure you want to delete this item?',
                                                'title' => 'Delete',
                                                'class' => 'btn btn-default',
                                                'data-pjax' => '0',
                                            ]);
                                    }
                                ],
                                ],
                        ],
                    ]); ?>
                    <?php //Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
<?php \yii\bootstrap\Modal::begin([
    'id' => 'activity-modal',
    'header' => '<h4 class="modal-title"></h4>',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => false],
    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">ปิด</a>',
]);
\yii\bootstrap\Modal::end();
?>