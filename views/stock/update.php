<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Stock */

$this->title = 'Update Stock: ' . $model->stock_id;
$this->params['breadcrumbs'][] = ['label' => 'Stocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="stock-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
