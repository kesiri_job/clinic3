<?php

use app\models\Profile;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stocks';
$this->params['breadcrumbs'][] = $this->title;
?>



<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php
$this->registerJs('
        function init_click_handlers(){
            $("#activity-create-link").click(function(e) {
                    $.get(
                        "create",
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                             $(".modal-title").html("เพิ่มข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });
            $(".activity-view-link").click(function(e) {
                    var fID = $(this).closest("tr").data("key");
                    $.get(
                        "view",
                        {
                            id: fID
                        },
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                            $(".modal-title").html("เปิดดูข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });
            $(".activity-update-link").click(function(e) {
                    var fID = $(this).closest("tr").data("key");
                    $.get(
                        "update",
                        {
                            id: fID
                        },
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                            $(".modal-title").html("ปรับปรุงข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });     
        }
        init_click_handlers(); //first run
        $("#customer_pjax_id").on("pjax:success", function() {
          init_click_handlers(); //reactivate links in grid after pjax update
        });'); ?>
<div class="col-lg-12">

    <div class="portlet">
        <div class="portlet-heading ">
            <h2 class="portlet-title text-dark">
                <?= Html::encode($this->title) ?>
            </h2>
            <div class="portlet-widgets">
                <?php /*Html::button('<i class="zmdi zmdi-collection-plus"></i> Create Stock', [ 'class' => 'btn btn-success','id'=>'activity-create-link']); */ ?>
                <?php //Html::a('<i class="fas fa-minus-square"></i> นำออก', ['stock-in'], ['class' => 'btn btn-purple','data-toggle'=>'tooltip','title'=>'นำออก','data-placement'=>'top']) ?>

                <?= Html::a('<i class="zmdi zmdi-collection-plus"></i> เพิ่มยา', ['stock-in'], ['class' => 'btn btn-success','data-toggle'=>'tooltip','title'=>'เพิ่มข้อมูลยาลงในระบบ','data-placement'=>'top']) ?>

                <?= Html::a('<i class="zmdi zmdi-collection-plus"></i> เพิ่มสต็อกยา', ['create'], ['class' => 'btn btn-success','data-toggle'=>'tooltip','title'=>'มีข้อมูลยาแล้วให้เพิ่มสต็อกยาที่นี่','data-placement'=>'top']) ?>

            </div>
            <?php \yii\bootstrap\Modal::begin([
                'id' => 'activity-modal',
                'header' => '<h4 class="modal-title"></h4>',
                'size' => 'modal-lg',
                'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">ปิด</a>',
            ]);
            \yii\bootstrap\Modal::end();
            ?>
        </div>
        <div id="bg-primary" class="panel-collapse collapse in">
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-10">
                        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
                    <div class="col-md-2">
                        <br/><br/><br/>
                        <div align="right">
                            <?php
                            $gridColumns = [
                                ['class' => 'kartik\grid\SerialColumn'],
                                'code',
                                'stock_date',
                                'stock_name',
                                'stock_number',
                                'left',
                                [
                                    'attribute' => 'stock_detail',
                                    'width' => '190px',
                                    'value' => function ($model, $key, $index, $widget) {
                                        return Html::a($model->stock_detail, '#', []);
                                    },
                                    'format' => 'html'
                                ],
                                [
                                    'attribute' => 'user_id',
                                    'width' => '190px',
                                    'value' => function ($model, $key, $index, $widget) {
                                       $user = Profile::findOne($model->user_id);
                                        return $user?$user->name:null;
                                    },
                                ],

                                /*  ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
                                      return '#';
                                  }]*/
                            ];

                            echo ExportMenu::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => $gridColumns,
                                'fontAwesome' => true,
                                'target' => '_blank',
                                'exportConfig' => [
                                    ExportMenu::FORMAT_TEXT => false,
                                    ExportMenu::FORMAT_PDF => false,
                                    ExportMenu::FORMAT_CSV => false,
                                ],
                            ]);


                            ?>
                        </div>
                    </div>
                </div>
                <?php Pjax::begin(['id' => 'pjax_1']); ?>
               <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'tableOptions' => [
                        'class' => 'table-responsive',
                    ],
                    'layout' => '{items}{summary}{pager}',
                    'columns' => [
                        ['class' => 'kartik\grid\SerialColumn', 'options' => ['style' => 'width:50px;'],],
                        [
                            'hAlign' => 'center',
                            'mergeHeader' => true,
                            'filter' => false,
                            'attribute' => 'code',
                        ],
                        [
                            'hAlign' => 'center',
                            'mergeHeader' => true,
                            'filter' => false,
                            'attribute' => 'stock_name',
                        ],
                        [
                            'hAlign' => 'center',
                            'mergeHeader' => true,
                            'filter' => false,
                            'attribute' => 'left',
                        ],
                        [
                            'hAlign' => 'center',
                            'mergeHeader' => true,
                            'filter' => false,
                            'attribute' => 'stock_number',
                        ],
                        [
                            'hAlign' => 'center',
                            'mergeHeader' => true,
                            'filter' => false,
                            'attribute' => 'stock_date',
                            'value' => function ($data) {
                                return \app\controllers\GetpublicController::getDateThaiTime($data->stock_date);
                            }
                        ],
                        [
                            'hAlign' => 'center',
                            'label' => 'สถานะ',
                            'attribute' => 'status',
                            'format' => 'html',
                            'filter' => $searchModel->itemStatus,
                            'value' => function ($model, $key, $index, $column) {
                                return $model->status == 1 ? "<span class='label label-primary'>Active</span>" : "<span class='label label-default'>Not Active</span>";
                            },
                            'filterType' => GridView::FILTER_SELECT2,
                            'filterWidgetOptions' => [
                                'pluginOptions' => ['allowClear' => true],
                            ],
                            'filterInputOptions' => ['placeholder' => 'สถานะ'],
                        ],
                        //'docter_id',
                        //'customer_id',

                        ['class' => 'kartik\grid\ActionColumn',
                            'template' => '<div class="btn-group btn-group-sm text-center" role="group">{export}{plus} {view} {update} {delete} </div>',
                            'options' => ['style' => 'width:200px;'],
                            'buttons' => [
                                'plus' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-plus-circle"></i>', ['plus', 'id' => $model->stock_id], [
                                        'class' => 'activity-export-link btn btn-purple',
                                        'title' => 'นำยาเข้า',
                                        'data-target' => '#activity-modal',
                                    ]);
                                },
                                'export' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-minus-circle"></i>', ['export', 'id' => $model->stock_id], [
                                        'class' => 'activity-export-link btn btn-danger',
                                        'title' => 'นำยาออก',
                                        'data-target' => '#activity-modal',
                                    ]);
                                },
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', ['view', 'id' => $model->stock_id], [
                                        'class' => 'activity-view-link btn btn-default',
                                        'title' => 'View',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#activity-modal',
                                        'data-id' => $key,
                                        'data-pjax' => '0',
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', ['update', 'id' => $model->stock_id, 'active' => '1'], [
                                        'class' => 'btn btn-default',
                                        'title' => 'Update',
                                        'data-id' => $key,
                                        'data-pjax' => '0',
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->stock_id],
                                        ['data-method' => 'post',
                                            'data-confirm' => 'Are you sure you want to delete this item?',
                                            'title' => 'Delete',
                                            'class' => 'btn btn-default',
                                            'data-pjax' => '0',
                                        ]);
                                }
                            ],

                        ],
                    ],
                ]); ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs(
    ' $(\'[data-toggle="tooltip"]\').tooltip();
		$("#grid-lesson-pjax").on("pjax:complete", function() {
			 $(\'[data-toggle="tooltip"]\').tooltip();
			 jQuery(\'.krajee-datepicker\').kvDatepicker(kvDatepicker_44c07868);
		});
    '
);
?>