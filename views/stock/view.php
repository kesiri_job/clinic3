<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Stock */

$this->title = $model->stock_id;
$this->params['breadcrumbs'][] = ['label' => 'Stocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'รหัส Bar Code',
                'value' => $model->code,
            ],
            [
                'label' => 'ชื่อ',
                'value' => $model->stock_name,
            ],
            [
                'label' => 'จำนวนนำเข้า',
                'value' => $model->stock_number,
            ],
            [
                'label' => 'จำนวนที่เหลือ',
                'value' => $model->left,
            ],
            [
                'label' => 'วันที่',
                'value' => \app\controllers\GetpublicController::getDateThaiTime($model->stock_date),
            ],
            [
                'label' => 'ชื่อผู้เบิกยา',
                'value' => \app\models\Profile::findOne($model->user_id)->name,
            ],
            [
                'label' => 'รายละเอียด',
                'format'=> 'html',
                'value' => $model->stock_detail,
            ],
            [
                'label' => 'สถานะ',
                'value' => empty($model->statusName)?null:$model->statusName,
            ],
        ],
    ]) ?>

</div>
