<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/23/2018
 * Time: 2:02 PM
 */
$this->title = 'Add Drugs';
$this->params['breadcrumbs'][] = ['label' => 'Stocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-12">

    <div class="portlet">
        <div class="portlet-heading ">
            <h2 class="portlet-title text-dark">
                <?= \yii\helpers\Html::encode($this->title) ?>
            </h2>
            <div class="portlet-widgets">

            </div>

        </div>
        <div id="bg-primary" class="panel-collapse collapse in">
            <div class="portlet-body" style="padding: 50px;">
                <div class="row">
                    <form id="target">
                        <div class="col-md-4">
                            <b>Bar Code</b><br/>
                            <input class="form-control" type="text" name="code" id="code"
                                   placeholder="กรุณาสแกนรหัสบาร์โค้ด" required>
                        </div>
                        <div class="col-md-6">
                            <b>ชื่อยา</b><br/>
                            <input class="form-control" type="text" name="name" id="name"
                                   placeholder="กรุณาใส่ชื่อยา"
                                   required>
                        </div>
                        <div class="col-md-2">
                            <br/>
                            <input class="btn btn-success m-b-5" type="submit" value="บันทึก">
                        </div>
                    </form>
                    <br/>
                    <br/>
                    <ol>
                    </ol>
                    <br/><br/><br/>
                    <label>รายชื่อยา</label>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th align="center"><center>#</center></th>
                            <th>รหัส Bar Code</th>
                            <th>ชื่อยา</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        foreach ($model as $item => $value) {
                            $item = $item+1;
                            echo " <tr><td align='center'>" . $item . "</td>" . "<td>" . $value->code . "</td><td>" . $value->name . "</td></tr>";
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs('
   $( "#target" ).submit(function( event ) {
       //alert( \'บันทึกยาสำเร็จ\' );

        $.ajax({
            url: \'' . Yii::$app->request->baseUrl . '/stock/stock-in\',
            type: \'post\',
            data: {
                code: $("#code").val(),
                name: $("#name").val(),
                _csrf: \'' . Yii::$app->request->getCsrfToken() . '\'
            },
            success: function (data) {
            if(data){
             // $("ol").append("<li> <b>Bar Code</b> "+$("#code").val()+" <b> ชื่อยา </b>"+$("#name").val()+"</li>");
           console.log(data);
           console.log(typeof(data));
            }else{
              console.log(\'Not\');
              alert(\'มียารหัสนี้อยู่ในระบบแล้ว\');
            }
             
            }
        });
     
        event.preventDefault();
     });
');
?>

