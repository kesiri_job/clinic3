<?php

use app\models\Profile;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Stock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stock-form">

    <div class="col-lg-12">
        <div class="portlet">
            <div class="portlet-heading ">
                <h2 class="portlet-title text-dark">
                    <?= empty($_GET['active']) ? "เพิ่มข้อมูล" : "แก้ไขข้อมูล" ?>
                </h2>
            </div>
            <div id="bg-primary" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'code')->textInput(['maxlength' => true,'id'=>'code'])->label('รหัส Bar Code') ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                        <?= $form->field($model, 'stock_name')->textInput(['maxlength' => true,'id'=>'name'])->label('ชื่อ') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'stock_date')->widget(\kartik\widgets\DatePicker::classname(), [
                                'options' => ['placeholder' => 'Enter date ...'],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ])->label('วันที่'); ?>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
                            'data' =>  ArrayHelper::map(\app\models\Userabt::find()->innerJoin(['profile', 'user.id = profile.user_id'])->where(['status'=>'1'])->all(),'profile.user_id','profile.name'),
                            'options' => ['placeholder' => 'เลือกผู้เบิกยา'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('ชื่อผู้เบิกยา'); ?>
                    </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'left')->textInput(['type' => 'number']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'stock_number')->textInput(['type' => 'number']) ?>
                        </div>
                    </div>
                    <?php echo $form->field($model, 'stock_detail')->widget(\dosamigos\tinymce\TinyMce::className(), [
                        'options' => ['rows' => 6],
                        'language' => 'es',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ])->label('รายละเอียด'); ?>
                    <?php //$form->field($model, 'stock_detail')->textarea(['maxlength' => true])->label('หมายเหตุ') ?>
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Not Active'])->label('สถานะ') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs('
$(\'input\').on("keypress", function(e) {
            /* ENTER PRESSED*/
            if (e.keyCode == 13) {
            
            $.ajax({
            url: \'' . Yii::$app->request->baseUrl . '/stock/create\',
            type: \'post\',
            data: {
                code: $("#code").val(),
                _csrf: \'' . Yii::$app->request->getCsrfToken() . '\'
            },
            success: function (data) {
            if(data){
             // $("ol").append("<li> <b>Bar Code</b> "+$("#code").val()+" <b> ชื่อยา </b>"+$("#name").val()+"</li>");
           $(\'#name\').val($(\'#name\').val() + data);
           console.log(data);
            }else{
              console.log(\'Not\');
              alert(\'มียารหัสนี้อยู่ในระบบแล้ว\');
            }
            }
        });
            
                /* FOCUS ELEMENT */
                var inputs = $(this).parents("form").eq(0).find(":input");
                var idx = inputs.index(this);

                if (idx == inputs.length - 1) {
                    inputs[0].select()
                } else {
                    inputs[idx + 1].focus(); //  handles submit buttons
                    inputs[idx + 1].select();
                }
                return false;
            }
        });
');
?>
