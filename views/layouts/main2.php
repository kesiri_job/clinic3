<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<div class="wrap">
    <aside class="left-panel">
        <div class="logo">
            <a href="<?php Yii::$app->homeUrl ?>" class="logo-expanded">
                <i class="ion-social-buffer"></i>
                <span class="nav-label">Cher Charm</span>
            </a>
        </div>
        <nav class="navigation">
            <ul class="list-unstyled">
                <li class="">
                    <a href="index.html"><i class="zmdi zmdi-view-dashboard"></i> <span class="nav-label">Dashboard</span></a>
                </li>
                <li class="has-submenu">
                    <a href="#"><i class="zmdi zmdi-format-underlined"></i> <span class="nav-label">User Interface</span><span class="menu-arrow"></span></a>
                </li>
            </ul>
        </nav>
    </aside>
    <section class="content">
        <!-- Header -->
        <header class="top-head container-fluid">
            <button type="button" class="navbar-toggle pull-left">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Left navbar -->
            <nav class=" navbar-default" role="navigation">
                <!-- Right navbar -->
                <ul class="nav navbar-nav navbar-right top-menu top-right-menu">
                    <!-- Notification -->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="far fa-bell"></i>
                            <span class=""></span>
                        </a>
                        <ul class="dropdown-menu extended fadeInUp animated nicescroll" tabindex="5002">
                            <li class="noti-header">
                                <p>Notifications</p>
                            </li>
                            <li>
                                <a href="#">
                                    <span style="color: #9e0505"><small class="text-muted">nothing</small></span>
                                </a>
                            </li>
                            <li>
                                <p><a href="#" class="text-right">See all notifications</a></p>
                            </li>
                        </ul>
                    </li>
                    <!-- /Notification -->

                    <!-- user login dropdown start-->
                    <li class="dropdown text-center">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="username"><?= Yii::$app->user->isGuest ? null: Yii::$app->user->identity->username ?> </span> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu pro-menu fadeInUp animated" tabindex="5003" style="overflow: hidden; outline: none;">
                            <?php
                            echo Nav::widget([
                                'options' => ['class' => 'navbar-nav navbar-right'],
                                'items' => [
                                    ['label' => 'Location', 'url' => ['/location/index']],
                                    Yii::$app->user->isGuest ?
                                        ['label' => 'Sign in', 'url' => ['/user/security/login']] :
                                    ['label' => 'Profile', 'url' => ['/user/settings/profile']],
                                    ['label' => 'Account', 'url' => ['/user/settings/account']],
                                    ['label' => 'Logout', 'url' => ['/user/security/logout'],'linkOptions' => ['data-method' => 'post']],
                                    ['label' => '', 'items'=>[
                                        ['label' => 'Logout', 'url' => ['/user/security/logout']],
                                    ]],
                                    ['label' => 'Register', 'url' => ['/user/registration/register'], 'visible' => Yii::$app->user->isGuest],
                                ],
                            ]);
                            ?>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- End right navbar -->
            </nav>
        </header>
    <?php $this->beginBody() ?>
    <div class="container">
    <br/><br/><br/><br/>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="portlet-heading">
                        <h3 class="panel-title"><?php // Html::encode( $this->title ) ?></h3>
                    </div>
                    <div class="panel-body">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </section>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
