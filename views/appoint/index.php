<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\models\AppointmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Appointments';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$this->registerJs('
        function init_click_handlers(){
            $("#activity-create-link").click(function(e) {
                    $.get(
                        "create",
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                             $(".modal-title").html("เพิ่มข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });
            $(".activity-view-link").click(function(e) {
                    var fID = $(this).closest("tr").data("key");
                    $.get(
                        "view",
                        {
                            id: fID
                        },
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                            $(".modal-title").html("เปิดดูข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });
            $(".activity-update-link").click(function(e) {
                    var fID = $(this).closest("tr").data("key");
                    $.get(
                        "update",
                        {
                            id: fID
                        },
                        function (data)
                        {
                            $("#activity-modal").find(".modal-body").html(data);
                            $(".modal-body").html(data);
                            $(".modal-title").html("ปรับปรุงข้อมูล");
                            $("#activity-modal").modal("show");
                        }
                    );
                });     
        }
        init_click_handlers(); //first run
        $("#customer_pjax_id").on("pjax:success", function() {
          init_click_handlers(); //reactivate links in grid after pjax update
        });'); ?>
<div class="appointment-index">
    <div class="col-lg-12">
        <div class="portlet">
            <div class="portlet-heading ">
                <h2 class="portlet-title text-dark">
                    <?= Html::encode($this->title) ?>
                </h2>
                <div class="portlet-widgets">
                    <p>
                        <?php // Html::button('<i class="zmdi zmdi-collection-plus"></i> เพิ่มการนัดพบ', [ 'class' => 'btn btn-success','id'=>'activity-create-link']); ?>
                        <a href="<?=Yii::getAlias('@web/site/calender')?>" class="btn btn-pink"><i class="fas fa-calendar-alt"></i> ปฎิทิน</a>
                        <?php echo Html::a('<i class="zmdi zmdi-collection-plus"></i> เพิ่มการนัดพบ', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
            </div>
            <div id="bg-primary" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-10">
                            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                        </div>
                        <div class="col-md-2">
                            <div align="right">
                                <br/><br/>
                                <?php
                                $gridColumns = [
                                    ['class' => 'kartik\grid\SerialColumn'],
                                    'appoint_date',
                                    'appoint_name',
                                    [
                                        'attribute' => 'customer_id',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            $customer = \app\models\Customer::findOne($model->customer_id);
                                            return $customer?$customer->customer_name:null;
                                        },
                                        'format' => 'raw'
                                    ],
                                    'type_pay',
                                    'deposit',
                                    'full_price',
                                    [
                                        'attribute' => 'appoint_detail',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->appoint_detail?$model->appoint_detail:null;
                                        },
                                        'format' => 'html'
                                    ],
                                    [
                                        'attribute' => 'status',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->getStatusName();
                                        },
                                        'format' => 'html'
                                    ],
                                    [
                                        'attribute' => 'call',
                                        'width' => '190px',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->getCallName();
                                        },
                                        'format' => 'html'
                                    ],

                                    /*  ['class' => 'kartik\grid\ActionColumn', 'urlCreator' => function () {
                                          return '#';
                                      }]*/
                                ];

                                echo ExportMenu::widget([
                                    'dataProvider' => $dataProvider,
                                    'columns' => $gridColumns,
                                    'fontAwesome' => true,
                                    'target' => '_blank',
                                    'exportConfig' => [
                                        ExportMenu::FORMAT_TEXT => false,
                                        ExportMenu::FORMAT_PDF => false,
                                        ExportMenu::FORMAT_CSV => false,
                                    ],
                                ]);


                                ?>
                            </div>
                        </div>
                    </div>
                    <?php \yii\widgets\Pjax::begin(['id' => 'pjax_1']); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'responsiveWrap' => false,
                        'layout' => '{items}{summary}{pager}',
                        'columns' => [
                            ['class' => 'kartik\grid\SerialColumn'],
                            [
                                'hAlign' => 'center',
                                'mergeHeader' => true,
                                'filter' => false,
                                'attribute' => 'appoint_name',
                                'value' => function ($data) {
                                    return $data->appoint_name;
                                },
                            ],
                            [
                                'hAlign' => 'center',
                                'mergeHeader' => true,
                                'filter' => false,
                                'attribute' => 'appoint_date',
                                'value' => function ($data) {
                                    return \app\controllers\GetpublicController::getDateThai($data->appoint_date);
                                },
                            ],
                            [
                                'hAlign' => 'center',
                                'mergeHeader' => true,
                                'filter' => false,
                                'format' => 'html',
                                'attribute' => 'customer_id',
                                'value' => function ($data) {
                                    if ($data->customer_id != null) {
                                        $customer = \app\models\Customer::find()->where(['customer_id' => $data->customer_id])->one()->customer_name;
                                        if ($customer != null) {
                                            $datac = "<a href='" . Yii::getAlias('@web') . "/customer/$data->customer_id'><mark>" . $customer . "</mark></a>";
                                        } else {
                                            $datac = 'ข้อมูลผิดพลาด';
                                        }
                                    } else {
                                        $datac = null;
                                        $customer = null;
                                    }
                                    return $datac;
                                },
                            ],
                            [
                                'hAlign' => 'center',
                                'attribute' => 'status',
                                'format' => 'html',
                                'filter' => $searchModel->itemStatus,
                                'value' => function ($model, $key, $index, $column) {
                                    return $model->status == 1 ? "<span class='label label-primary'>เข้าพบแล้ว</span>" : "<span class='label label-default'>ยังไม่เข้าพบ</span>";
                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => ['placeholder' => 'สถานะ'],
                            ],
                            [
                                'hAlign' => 'center',
                                'header' => 'สถานะการโทร',
                                'attribute' => 'call',
                                'format' => 'html',
                                'filter' => $searchModel->itemCall,
                                'value' => function ($model, $key, $index, $column) {
                                    return $model->call == 1 ? "<span class='label label-success'><i class='fas fa-phone'></i> โทรตามแล้ว</span>" : "<span class='label label-default'><i class='fas fa-phone'></i> ยังไม่โทรตาม</span>";
                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => ['placeholder' => 'สถานะ'],
                            ],
                            ['class' => 'kartik\grid\ActionColumn',
                                'options' => ['style' => 'width:140px;'],
                                'buttonOptions' => ['class' => 'btn btn-default'],
                                'template' => '<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} {delete} </div>',
                            ],
                        ],
                    ]); ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php \yii\bootstrap\Modal::begin([
    'id' => 'activity-modal',
    'header' => '<h4 class="modal-title"></h4>',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => false],
    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">ปิด</a>',
]);
\yii\bootstrap\Modal::end();
?>
