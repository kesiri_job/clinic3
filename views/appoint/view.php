<?php

use app\models\Customer;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Appointment */

$this->title = $model->appoint_id;
$this->params['breadcrumbs'][] = ['label' => 'Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = empty($customer1) ? $customer1= Customer::find()->where(['customer_id'=>$model->customer_id])->one()->customer_name :$this->title;
?>
<div class="appointment-view">

    <div class="portlet">

        <div id="bg-primary" class="panel-collapse collapse in">
            <div class="portlet-body">
                <div class="table-responsive">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>ดูข้อมูล</h4>
                        </div>
                        <div class="col-md-6" align="right">
                            <?= Html::a('<i class="fas fa-edit"></i> Update', ['update', 'id' => $model->appoint_id], ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>
                    <br/>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'appoint_name',
                            [
                                'label' => 'วันที่และเวลานัดพบ',
                                'value' => \app\controllers\GetpublicController::getDateThai($model->appoint_date),
                            ],
                            [
                                'label' => 'ชื่อผู้เข้าพบ',
                                'value' => empty($customer) ? $customer= Customer::find()->where(['customer_id'=>$model->customer_id])->one()->customer_name :null,
                            ],
                            'type_pay',
                            'deposit',
                            'full_price',
                            'appoint_detail',
                            [
                                'label' => 'สถานะ',
                                'value' => empty($model->statusName)?null:$model->statusName,
                            ],
                            [
                                'label' => 'สถานะการโทรตาม',
                                'value' => empty($model->callName)?null:$model->callName,
                            ],
                        ],
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</div>

