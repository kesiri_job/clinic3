<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Appointment */

$this->title = 'Update Appointment: ' . $model->appoint_id;
$this->params['breadcrumbs'][] = ['label' => 'Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="appointment-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
