<?php

use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Used */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="used-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-9">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'date')->widget(\kartik\widgets\DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter date ...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label('วันที่'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <?= $form->field($model, 'user_id')
                ->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\app\models\Profile::find()->all(), 'user_id', 'name'),
                    'options' => ['placeholder' => 'เลือกชื่อผู้เบิกยา'],
                    'pluginOptions' => [
                        'allowClear' => false,
                    ],
                ])->label('ผู้เบิกยา'); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'number')->textInput(['type' => 'number']) ?>
        </div>
    </div>
    <?php echo $form->field($model, 'detail')->widget(\dosamigos\tinymce\TinyMce::className(), [
        'options' => ['rows' => 3],
        'language' => 'es',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ])->label('รายละเอียด'); ?>
    <div class="form-group" align="right">
        <?= Html::submitButton('<i class="fa fa-save"></i>  Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
