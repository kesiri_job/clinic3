<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Used */

$this->title = 'Update Used: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Useds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="used-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
