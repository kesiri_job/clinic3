<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Used */

$this->title = 'Create Used';
$this->params['breadcrumbs'][] = ['label' => 'Useds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="used-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
