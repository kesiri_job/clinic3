<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Used */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Useds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="used-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'user_id',
            'number',
            [
                'format' => 'html',
                'label' => 'รายละเอียด',
                'value' => $model->detail,
            ],
        ],
    ]) ?>

</div>
