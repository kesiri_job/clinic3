<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Expend */

$this->title = 'Update Expend: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Expends', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="expend-update">
    <?= $this->render('expend_form', [
        'model' => $model,
    ]) ?>

</div>
