<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Expend */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Expends', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expend-view">
    <h4><b>รายจ่ายวันที่ </b> <?=$model->expend_date?\app\controllers\GetpublicController::getDateThaiTime($model->expend_date):null?></h4>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'expend_name',
            'expend_number',
            [
                'format'=>'html',
                'attribute' => 'detail',
                'value' => $model->detail?$model->detail:null,
            ],
            [
                'attribute' => 'Create By',
                'value' => $model->created_by?\app\models\Userabt::findOne($model->created_by)->username:null,
            ],
            [
                'attribute' => 'Update By',
                'value' => $model->updated_by?\app\models\Userabt::findOne($model->updated_by)->username:null,
            ],

        ],
    ]) ?>

</div>
