<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IncomeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="income-search">

    <?php $form = ActiveForm::begin([
        'action' => ['income'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'income_name')->textInput(['placeholder' => 'Search Name'])->label('') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'income_date')->widget(\kartik\widgets\DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter date'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(''); ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('<i class="fas fa-search"></i> Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
