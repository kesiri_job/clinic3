<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Expend */

$this->title = 'Create Expend';
$this->params['breadcrumbs'][] = ['label' => 'Expends', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expend-create">
    <?= $this->render('expend_form', [
        'model' => $model,
    ]) ?>

</div>
