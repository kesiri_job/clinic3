<?php

use app\controllers\GetpublicController;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Income */

$this->title = $model->income_id;
$this->params['breadcrumbs'][] = ['label' => 'Incomes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="income-view">
<!--    <div align="right">-->
<!--        <p>-->
<!--            --><?php // Html::a('<i class="fas fa-edit"></i> Update', ['income-update', 'id' => $model->income_id], ['class' => 'btn btn-success']) ?>
<!--        </p>-->
<!--    </div>-->
    <h4><b>รายรับวันที่ </b> <?=$model->income_date?GetpublicController::getDateThaiTime($model->income_date):null?></h4>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'income_name',
            'income_number',
            [
                    'format'=>'html',
                'attribute' => 'detail',
                'value' => $model->detail?$model->detail:null,
            ],
            [
                'attribute' => 'Create By',
                'value' => $model->created_by?\app\models\Userabt::findOne($model->created_by)->username:null,
            ],
            [
                'attribute' => 'Update By',
                'value' => $model->updated_by?\app\models\Userabt::findOne($model->updated_by)->username:null,
            ],

        ],
    ]) ?>

</div>
