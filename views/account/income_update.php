<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Income */

$this->title = 'Update Income: ' . $model->income_id;
$this->params['breadcrumbs'][] = ['label' => 'Incomes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->income_id, 'url' => ['view', 'id' => $model->income_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="income-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
