<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExpendSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="expend-search">

    <?php $form = ActiveForm::begin([
        'action' => ['expend'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'expend_name')->textInput(['placeholder' => 'Search Name'])->label('') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'expend_date')->widget(\kartik\widgets\DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter date'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(''); ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
