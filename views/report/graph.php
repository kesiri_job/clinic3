<?php
use app\controllers\GetpublicController;
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\HighchartsAsset;
use yii\web\JsExpression;
echo "<br/>";
echo "<blockquote><p>";
echo '<h5><b>ค้นหาช่วงวันที่</b></p><footer> '.GetpublicController::getDateThaiTime($formdate).'<b> ถึงวันที่ </b>'.GetpublicController::getDateThaiTime($todate).'</footer></h5>';
echo "</blockquote>";
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'กราฟแสดงสถานะการตอบรับ',
        ],
        'xAxis' => [
            'categories' => [ 'สถานะ'],
        ],
        'labels' => [
            'items' => [
                [
                    'html' => 'เปอร์เซ็นสถานะ',
                    'style' => [
                        'left' => '50px',
                        'top' => '18px',
                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                    ],
                ],
            ],
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'รอตอบรับ',
                'data' => [(int)$status2],
            ],
            [
                'type' => 'column',
                'name' => 'รอเซ็นสัญญา',
                'data' => [(int)$status3],
            ],
            [
                'type' => 'column',
                'name' => 'เซ็นสัญญา',
                'data' => [(int)$status4],
            ],
            [
                'type' => 'column',
                'name' => 'ยกเลิกสัญญา',
                'data' => [(int)$status5],
            ],
            [
                'type' => 'column',
                'name' => 'ไม่ตอบรับ',
                'data' => [(int)$status6],
            ],
            [
                'type' => 'pie',
                'name' => 'ร้อยละ',
                'data' => [
                    [
                        'name' => 'รอตอบรับ',
                        'y' => $per2,
                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
                    ],
                    [
                        'name' => 'รอเซ็นสัญญา',
                        'y' => $per3,
                        'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
                    ],
                    [
                        'name' => 'เซ็นสัญญา',
                        'y' => $per4,
                        'color' => new JsExpression('Highcharts.getOptions().colors[2]'), // Joe's color
                    ],
                    [
                        'name' => 'ยกเลิกสัญญา',
                        'y' => $per5,
                        'color' => new JsExpression('Highcharts.getOptions().colors[3]'), // Jane's color
                    ],
                    [
                        'name' => 'ไม่ตอบรับ',
                        'y' => $per6,
                        'color' => new JsExpression('Highcharts.getOptions().colors[4]'), // Jane's color
                    ],
                ],
                'center' => [42, 80],
                'size' => 120,
                'showInLegend' => false,
                'dataLabels' => [
                    'enabled' => false,
                ],
            ],
        ],
    ]
]);


//echo $status2." : ".$status3." : ".$status4." : ".$size." : ".$per2;

?>

