<?php
use app\controllers\GetpublicController;
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\HighchartsAsset;
use yii\web\JsExpression;
use yii\web\View;


echo "<br/>";
echo "<blockquote><p>";
echo '<h5><b>ค้นหาช่วงวันที่</b></p><footer> ทั้งหมด </footer></h5>';
echo "</blockquote>";
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'กราฟแสดงสถานะการติดต่อ',
        ],
        'xAxis' => [
            'categories' => ['สถานะ'],
        ],
        'labels' => [
            'items' => [
                [
                    'html' => 'เปอร์เซ็นสถานะ',
                    'style' => [
                        'left' => '50px',
                        'top' => '18px',
                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                    ],
                ],
            ],
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'ยังไม่ติดต่อ',
                'data' => [0],
                'color' => new JsExpression('Highcharts.getOptions().colors[5]'), // Jane's color
                'showInLegend'=> false

            ],
            [
                'type' => 'column',
                'name' => 'กำลังติดต่อโทรประสาน',
                'data' => [$status2],
            ],
            [
                'type' => 'column',
                'name' => 'กำลังติดต่อวิ่งส่งเอกสาร',
                'data' => [$status3],
            ],
            [
                'type' => 'column',
                'name' => 'รอตอบรับหนังสือ',
                'data' => [$status4],
            ],
            [
                'type' => 'column',
                'name' => 'รอเข้าพบ',
                'data' => [$status5],
            ],
            [
                'type' => 'column',
                'name' => 'เข้าพบแล้ว',
                'data' => [$status6],
                'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
            ],
            [
                'type' => 'column',
                'name' => 'ปฎิเสธการเข้าพบ',
                'data' => [$status7],
            ],

            [
                'type' => 'pie',
                'name' => 'ร้อยละ',
                'data' => [
                    [
                        'name' => 'ยังไม่ติดต่อ',
                        'y' => 0,
                        'color' => new JsExpression('Highcharts.getOptions().colors[5]'), // Jane's color
                    ],
                    [
                        'name' => 'กำลังติดต่อโทรประสาน',
                        'y' => $per2,
                        'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
                    ],
                    [
                        'name' => 'กำลังติดต่อวิ่งส่งเอกสาร',
                        'y' => $per3,
                        'color' => new JsExpression('Highcharts.getOptions().colors[2]'), // Joe's color
                    ],
                    [
                        'name' => 'รอตอบรับหนังสือ',
                        'y' => $per4,
                        'color' => new JsExpression('Highcharts.getOptions().colors[3]'), // Joe's color
                    ],
                    [
                        'name' => 'รอเข้าพบ',
                        'y' => $per5,
                        'color' => new JsExpression('Highcharts.getOptions().colors[4]'), // Joe's color
                    ],
                    [
                        'name' => 'เข้าพบแล้ว',
                        'y' => $per6,
                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Joe's color
                    ],
                    [
                        'name' => 'ปฎิเสธการเข้าพบ',
                        'y' => $per7,
                        'color' => new JsExpression('Highcharts.getOptions().colors[6]'), // Joe's color
                    ],
                ],
                'center' => [50, 80],
                'size' => 120,
                'showInLegend' => false,
                'dataLabels' => [
                    'enabled' => false,
                ],
            ],
        ],
    ]
]);

$script = <<< JS
   var series = chart.series[0].hide();
    /*series.hide();*/
JS;
$this->registerJs($script,
    View::POS_READY,
    'my-option'
);



