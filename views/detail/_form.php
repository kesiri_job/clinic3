<?php


use app\models\Userabt;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\AbtDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="abt-detail-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'visitdate')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'th',
        'dateFormat' => 'yyyy-MM-dd',
        'clientOptions'=>[
            'changeMonth'=>true,
            'changeYear'=>true,
        ],
        'options'=>['class'=>'form-control']
    ])->label('วันที่เข้าพบ') ?>
    <?= $form->field( $model, 'title' )->textInput( ['maxlength' => true] ) ?>

   <?=  $form->field($modelDetail, 'users' )->widget( Select2::classname(), [
        'data' => ArrayHelper::map(\app\models\Profile::find()->all(), 'user_id', 'name' ),
        'options' => ['placeholder' => 'Choose Tools used' , 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
            'tokenSeparators' => [','],
            'maximumInputLength' => 30
        ],
    ] )->label('ผู้เข้าพบ');
  ?>

    <?= $form->field($model, 'dt_detail')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'language' => 'es',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
    <?= $form->field( $model, 'cw_running' )->hiddenInput( ['value' => $cw_id] )->label( false ) ?>
    <?php echo $form->field( $model, 'attach' )->widget( \trntv\filekit\widget\Upload::classname(), [
        'url' => ['upload'],
        'sortable' => true,
        'acceptFileTypes' => new JsExpression( '/(\.|\/)(pdf|doc|docx|ppt|pptx|jpeg|jpg|png|xls|xlsx)$/i' ),
        'maxNumberOfFiles' => 10
    ] )?>



    <div class="form-group">
        <?= Html::submitButton( 'บันทึก', ['class' => 'btn btn-success'] ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
