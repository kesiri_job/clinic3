<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AbtDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="abt-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'dt_running') ?>

    <?= $form->field($model, 'dt_detail') ?>

    <?= $form->field($model, 'cr_by') ?>

    <?= $form->field($model, 'cr_date') ?>

    <?= $form->field($model, 'upd_by') ?>

    <?php // echo $form->field($model, 'upd_date') ?>

    <?php // echo $form->field($model, 'ACTIVE') ?>

    <?php // echo $form->field($model, 'cw_running') ?>

    <?php // echo $form->field($model, 'title') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
