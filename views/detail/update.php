<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AbtDetail */

$this->title = 'แก้ไขข้อมูลการเข้าพบ: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลการเข้าพบ', 'url' => ['index', 'id' => $model->cw_running]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->dt_running]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="abt-detail-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'cw_id'=>$cw_id,
        'modelDetail'=> $modelDetail
    ]) ?>

</div>
