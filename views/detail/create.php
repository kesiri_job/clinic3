<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AbtDetail */
/* @var $cw_id integer */

$this->title = 'เพิ่มการเข้าพบ';
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลการเข้าพบ', 'url' => ['index','id'=>$cw_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="abt-detail-create">

    <h3><?= Html::encode($this->title) ?></h3>
<br/>
    <?= $this->render('_form', [
        'model' => $model,
        'cw_id'=>$cw_id,
        'modelDetail'=>$modelDetail
    ]) ?>

</div>
