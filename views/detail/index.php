<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AbtDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลการเข้าพบ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="abt-detail-index">

    <h3><?= Html::encode( $this->title ) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= Html::a( 'เพิ่มข้อมูลการเข้าพบ', ['create', 'id' => $id], ['class' => 'btn btn-success'] ) ?>
    </p>
    <br/><br/>
    <?= GridView::widget( [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{items}{summary}{pager}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'dt_running',
            // 'dt_detail',
            //  'cr_by',
            // 'cr_date',
            //  'upd_by',
            //'upd_date',
            //'ACTIVE',

            'title',
            'creator',

            ['class' => 'yii\grid\ActionColumn',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'template' => '<div class="btn-group btn-group-sm text-center" role="group"> {view} {update} {delete} </div>',
                'options' => ['style' => 'width:150px;'],
               ],
        ],
    ] ); ?>
</div>
