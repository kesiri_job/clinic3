<?php

use app\models\Fat;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Meeting */

$this->title = $model->meet_title;
$this->params['breadcrumbs'][] = ['label' => 'Meet', 'url' => ['meet/index/' . $customer]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-view">
    <div class="col-lg-12">
        <div class="tabs-vertical-env">
            <ul class="nav tabs-vertical">
                <li class="active">
                    <a href="#v-home" data-toggle="tab" aria-expanded="true">ดูข้อมูล</a>
                </li>
                <?php if (!empty($_GET['status'])) { ?>
                <li class="">
                    <a href="#v-profile" data-toggle="tab" aria-expanded="false">รายละเอียดเคสแก้ไข</a>
                </li>
                <?php }?>
                <li class="">
                    <a href="#v-messages" data-toggle="tab" aria-expanded="false">ภาพหลังทำการรักษา</a>
                </li>
            </ul>

            <div class="tab-content" style="width: 100%">
                <div class="tab-pane active" id="v-home">
                    <div class="table-responsive">
                        <div class="row" >
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4>ดูข้อมูล</h4>
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <?= Html::a('<i class="fas fa-edit"></i> Update', ['update', 'id' => $model->meet_id, 'customer' => $customer], ['class' => 'btn btn-success']) ?>
                                    </div>
                                </div>
                                <br/>
                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [

                                        [
                                            'label' => 'หัวข้อการเข้าพบ',
                                            'value' => $model->meet_title,
                                        ],
                                        [
                                            'label' => 'ชื่อ-นามสกุล',
                                            'value' => empty($model->customer_id) ? null : $model->customer->customer_name,
                                        ],
                                        [
                                            'label' => 'วันที่',
                                            'value' => \app\controllers\GetpublicController::getDateThai($model->meet_date),
                                        ],
                                        [
                                            'format' => 'html',
                                            'label' => 'รายการที่ทำ',
                                            'value' => empty($array) ? null : implode('<br/>', $array),
                                        ],
                                        [
                                            'format' => 'html',
                                            'label' => 'Fat',
                                            'value' => empty($arrayFat) ? null : implode('<br/>', $arrayFat),
                                        ],
                                        [
                                            'format' => 'html',
                                            'label' => 'Botox',
                                            'value' => empty($arrayBotox) ? null : implode('<br/>', $arrayBotox),
                                        ],
                                        [
                                            'label' => 'จำนวนเงินค่ารักษา',
                                            'value' => $model->summary,
                                        ],
                                        [
                                            'format' => 'html',
                                            'label' => 'รายละเอียด',
                                            'value' => $model->meet_detail,
                                        ],
                                    ],
                                ]); ?><br/>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (!empty($_GET['status'])) { ?>
                <div class="tab-pane" id="v-profile">

                    <div class="table-responsive">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>รายละเอียดเคสแก้ไข</h4>
                            </div>
                        </div>

                        <?= DetailView::widget([
                            'model' => $model,

                            'attributes' => [
                                [
                                    'format' => 'html',
                                    'label' => 'สถานะ',
                                    'value' => empty($model->statusName) ? null : "<b>" . $model->statusName . "</b>",
                                ],
                                [
                                    'format' => 'html',
                                    'label' => 'สถานะการรักษา',
                                    'value' => empty($model->statusName2) ? null : "<b>" . $model->statusName2 . "</b>",
                                ],
                                [
                                    'format' => 'html',
                                    'label' => 'ปัญหา',
                                    'value' => $model->proplem,
                                ],
                                [
                                    'format' => 'html',
                                    'label' => 'ผลลัพธ์',
                                    'value' => $model->result,
                                ],

                            ]

                        ]);

                        ?>

                    </div>
                </div>
                <?php } ?>
                <div class="tab-pane" id="v-messages">
                    <h4>รูปภาพหลังทำการรักษา</h4>
                    <img src="<?= $model->getImage() ?>" class="img-thumbnail" width="500">
                </div>
            </div>
        </div>



    </div>
</div>


