<?php

use app\models\Doctor;
use app\models\Fat;
use app\models\ListMeet;
use app\models\Meeting;
use kartik\datetime\DateTimePicker;
use trntv\filekit\widget\Upload;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->registerJsFile('@web/js/hide.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
/* @var $this yii\web\View */
/* @var $model app\models\Meeting */
/* @var $form yii\widgets\ActiveForm */
$js = '
   $(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
        console.log("beforeInsert");
    });

    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        console.log("afterInsert");
    });
  

    $(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
        if (! confirm("Are you sure you want to delete this item?")) {
            return false;
        }
        return true;
    });

    $(".dynamicform_wrapper").on("afterDelete", function(e) {
        console.log("Deleted item!");
    });

    $(".dynamicform_wrapper").on("limitReached", function(e, item) {
        alert("Limit reached");
    });
    
    
//01
   $(".dynamicform_wrapper2").on("beforeInsert", function(e, item2) {
        console.log("beforeInsert");
    });

    $(".dynamicform_wrapper2").on("afterInsert", function(e, item2) {
        console.log("afterInsert");
    });
  

    $(".dynamicform_wrapper2").on("beforeDelete", function(e, item2) {
        if (! confirm("Are you sure you want to delete this item?")) {
            return false;
        }
        return true;
    });

    $(".dynamicform_wrapper2").on("afterDelete", function(e) {
        console.log("Deleted item!");
    });

    $(".dynamicform_wrapper2").on("limitReached", function(e, item2) {
        alert("Limit reached");
    });
   
';


$this->registerJs($js);
?>

<div class="meeting-form">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
        <div class="portlet">
            <div class="portlet-heading ">
                <div class="row">
                    <div class="col-md-9">
                        <h2 class="portlet-title text-dark">
                            <?= empty($_GET['active']) ? "แก้ไขข้อมูล" : "เพิ่มข้อมูล" ?>
                            <?= empty($_GET['status']) ? "" : "/เคสแก้ไข" ?>
                        </h2>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'summary')->textInput(['maxlength' => true, 'type' => 'number', 'placeholder' => 'จำนวนเงิน ฿'])->label(false) ?>
                    </div>
                </div>
            </div>
            <div id="bg-primary" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <?php if(!empty($_GET['status'])){?>
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'status_2')->dropDownList(['0' => 'กำลังรักษา','1'=>'เสร็จสิ้น'])->label('สถานะ') ?>
                        </div>
                        <div class="col-md-9">
                            <?= $form->field($model, 'meet_id_edit')
                                ->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(Meeting::find()->where(['customer_id' => $customer])->all(), 'meet_id', 'meet_title'),
                                    'options' => ['placeholder' => 'เลือกการเข้าพบ'],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                    ],
                                ])->label('ชื่อเคสที่แก้ไข'); ?>
                            <?= $form->field($model, 'status')->hiddenInput(['value' => '0'])->label(false); ?>
                        </div>
                    </div>
                    <?php }else{?>
                        <?= $form->field($model, 'status')->hiddenInput(['value' => '1'])->label(false); ?>
                   <?php }?>
                    <div class="row">
                        <div class="col-md-9">
                            <?= $form->field($model, 'meet_title')->textInput(['maxlength' => true])->label('หัวข้อการเข้าพบ') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'meet_date')->widget(DateTimePicker::classname(), [
                                'options' => ['placeholder' => 'Enter event time ...'],
                                'pluginOptions' => [
                                    'autoclose' => true
                                ]
                            ])->label('วันที่/เวลา'); ?>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'doctor_id')
                                ->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(Doctor::find()->all(), 'doctor_id', 'doctor_name'),
                                    'options' => ['placeholder' => 'เลือกหมอ'],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                    ],
                                ])->label('หมอที่เข้าพบ'); ?>
                        </div>
                    </div>
                    <?= $form->field($model, 'lists')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(ListMeet::find()->all(), 'list_id', 'list_name'),
                        'options' => ['placeholder' => 'กรอกรายการ', 'multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true,
                            'tokenSeparators' => [','],
                            'maximumInputLength' => 30
                        ],
                    ])->label('รายการที่ทำ');
                    ?>
                    <div id="bofatid">
                        <br/>
                        <b>Fat</b>

                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $modelsFatlist[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'fat_id_name',
                                'fat_cc',
                            ],
                        ]); ?>


                        <div class="container-items"><!-- widgetContainer -->
                            <?php foreach ($modelsFatlist as $i => $modelsFatlist): ?>
                                <div class="item panel panel-default"><!-- widgetBody -->

                                    <div class="panel-body">
                                        <div class="pull-right">
                                            <button type="button" class="add-item btn btn-success btn-xs"><i
                                                        class="glyphicon glyphicon-plus"></i></button>
                                            <button type="button" class="remove-item btn btn-danger btn-xs"><i
                                                        class="glyphicon glyphicon-minus"></i></button>
                                        </div>
                                        <div class="clearfix"></div>
                                        <?php
                                        // necessary for update action.
                                        if (!$modelsFatlist->isNewRecord) {
                                            echo Html::activeHiddenInput($modelsFatlist, "[{$i}]id");
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <?= $form->field($modelsFatlist, "[{$i}]fat_id_name")->dropDownList(['หน้า' => 'หน้า', 'เหนียง' => 'เหนียง', 'ต้นแขน' => 'ต้นแขน', 'ต้นขา' => 'ต้นขา', 'หน้าท้อง' => 'หน้าท้อง','ขมับ'=>'ขมับ','ใต้ตา'=>'ใต้ตา','จมูก'=>'จมูก','ร่องแก้ม'=>'ร่องแก้ม','คาง'=>'คาง'], ['maxlength' => true])->label('Name') ?>

                                                <?php /*$form->field($modelsFatlist, "[{$i}]fat_id_name")
                                                ->widget(Select2::classname(), [
                                                    'data' =>['หน้า' => 'หน้า', 'เหนียง' => 'เหนียง', 'ต้นแขน' => 'ต้นแขน', 'ต้นขา' => 'ต้นขา', 'หน้าท้อง' => 'หน้าท้อง'],
                                                    'options' => ['placeholder' => 'เลือก Fat'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])->label('Fat');*/ ?>
                                            </div>
                                            <div class="col-sm-3">
                                                <?= $form->field($modelsFatlist, "[{$i}]fat_cc")->textInput(['maxlength' => true])->label('cc.') ?>
                                            </div>
                                        </div><!-- .row -->
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?php DynamicFormWidget::end(); ?>

                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper2', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items2', // required: css class selector
                            'widgetItem' => '.item2', // required: css class
                            'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $modelsBotoxlist[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'botox_id_name',
                                'botox_left',
                                'botox_right',
                                'botox_u'
                            ],
                        ]); ?>
                        <b>Botox</b>
                        <div class="container-items2"><!-- widgetContainer -->
                            <?php foreach ($modelsBotoxlist as $i => $modelsBotoxlist): ?>
                                <div class="item2 panel panel-default"><!-- widgetBody -->

                                    <div class="panel-body">
                                        <div class="pull-right">
                                            <button type="button" class="add-item btn btn-success btn-xs"><i
                                                        class="glyphicon glyphicon-plus"></i></button>
                                            <button type="button" class="remove-item btn btn-danger btn-xs"><i
                                                        class="glyphicon glyphicon-minus"></i></button>
                                        </div>
                                        <div class="clearfix"></div>
                                        <?php
                                        // necessary for update action.
                                        if (!$modelsBotoxlist->isNewRecord) {
                                            echo Html::activeHiddenInput($modelsBotoxlist, "[{$i}]id");
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <?= $form->field($modelsBotoxlist, "[{$i}]botox_id_name")->dropDownList(['กราม' => 'กราม', 'ลิฟกรอบหน้า' => 'ลิฟกรอบหน้า', 'Micro Botox' => 'Micro Botox', 'ลดกล้ามเนื้อแขน' => 'ลดกล้ามเนื้อแขน', 'ลกกล้ามเนื้อน่อง' => 'ลกกล้ามเนื้อน่อง', 'ลดปีกจมูก' => 'ลดปีกจมูก', 'หน้าผาก' => 'หน้าผาก', 'หว่างคิ้ว' => 'หว่างคิ้ว','หางตา'=>'หางตา','ใต้ตา'=>'ใต้ตา','ร่องน้ำหมาก'=>'ร่องน้ำหมาก'], ['maxlength' => true])->label('Name') ?>
                                            </div>
                                            <div class="col-sm-3">
                                                <?= $form->field($modelsBotoxlist, "[{$i}]botox_u")->textInput(['maxlength' => true])->label('cc.') ?>
                                            </div>
                                        </div><!-- .row -->
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?php DynamicFormWidget::end(); ?>
                    </div>
                    <?php echo $form->field($model, 'meet_detail')->widget(\dosamigos\tinymce\TinyMce::className(), [
                        'options' => ['rows' => 6],
                        'language' => 'es',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ])->label('รายละเอียด'); ?>
                    <?php if(!empty($_GET['status'])){?>
                        <?= $form->field($model, 'proplem')->widget(\dosamigos\tinymce\TinyMce::className(), [
                            'options' => ['rows' => 6],
                            'language' => 'es',
                            'clientOptions' => [
                                'plugins' => [
                                    "advlist autolink lists link charmap print preview anchor",
                                    "searchreplace visualblocks code fullscreen",
                                    "insertdatetime media table contextmenu paste"
                                ],
                                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                            ]
                        ])->label('ปัญหา'); ?>
                        <?= $form->field($model, 'result')->widget(\dosamigos\tinymce\TinyMce::className(), [
                            'options' => ['rows' => 6],
                            'language' => 'es',
                            'clientOptions' => [
                                'plugins' => [
                                    "advlist autolink lists link charmap print preview anchor",
                                    "searchreplace visualblocks code fullscreen",
                                    "insertdatetime media table contextmenu paste"
                                ],
                                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                            ]
                        ])->label('ผลลัพธ์'); ?>
                    <?php } ?>

                    <?= $form->field($model, 'file')->widget(Upload::classname(), ['url' => ['avatar-upload']])->label('รูปภาพ') ?>


                    <div class="form-group">
                        <br/><br/>
                        <?= Html::submitButton('<i class="fa fa-save"></i> Save', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>

            </div>
        </div>

