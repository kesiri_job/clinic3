<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Meeting */

$this->title = 'Create Meeting';
$this->params['breadcrumbs'][] = ['label' => 'Meet', 'url' => ['meet/index/'.$customer]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFatlist'=>$modelsFatlist,
        'modelsBotoxlist'=>$modelsBotoxlist,
        'customer'=>$customer
    ]) ?>

</div>
