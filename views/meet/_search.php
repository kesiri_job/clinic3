<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MeetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index','id'=>$customer_id],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'meet_title')->textInput(['placeholder' => 'Search Title'])->label(false) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'meet_date')->widget(\kartik\widgets\DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter date'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(false); ?>
        </div>
    </div>
    <div class="form-group">
    <?= Html::submitButton('<i class="fas fa-search"></i> Search', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
