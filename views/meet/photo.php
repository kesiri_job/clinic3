<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/7/2018
 * Time: 10:54 PM
 */
use app\controllers\GetpublicController;
use yii\helpers\Html;

$this->title = 'Photo';
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['customer/index']];
$this->params['breadcrumbs'][] = $this->title;
//$customer->customer_name
?>
<div class="portlet">
    <div class="portlet-heading ">
        <h2 class="portlet-title text-dark">
            <?= Html::encode($this->title) ?>
        </h2>
    </div>
    <div id="bg-primary" class="panel-collapse collapse in">
        <div class="portlet-body">
            <div class="row">

            <?php
            if($photo){
                $check = true;
                foreach ($photo as $hee){
                    if($hee->status=='0'){
                        $status="edit";
                    }else{
                        $status="";
                    }
                    if($hee->picture){
                        echo "<div class='col-md-6'><h4><a href='".Yii::getAlias('@web/meet/').$hee->meet_id."?customer=$id&status=$status'> ".GetpublicController::getDateThai($hee->meet_date)."</a></h4>";
                        echo "<div class='img-resize'><img src='".$hee->getImagesum()."' class='img-thumbnail' width='500'  height='300'></div>";
                        echo "  </div>";
                        $check = false;
                    }
                }
                if($check){
                    echo "<div class='col-md-6'>";
                    echo "<p class=\"text-muted\">None Picture.</p>";
                    echo "</div>";
                }
            }
            ?>
              </div>
        </div>
    </div>
</div>


