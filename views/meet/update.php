<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Meeting */

$this->title = 'Update Meeting: ' . $model->meet_id;
$this->params['breadcrumbs'][] = ['label' => 'Meet', 'url' => ['meet/index/'.$customer]];
$this->params['breadcrumbs'][] = 'Update :: '.$name;
?>
<div class="meeting-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFatlist'=>$modelsFatlist,
        'modelsBotoxlist'=>$modelsBotoxlist,
        'customer'=>$customer
    ]) ?>

</div>
